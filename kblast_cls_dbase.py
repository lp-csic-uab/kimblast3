# !/usr/bin/env python
# -*- coding: iso-8859-1 -*-
#
"""
kblast_cls_dbase
"""
#
import os
import sqlite3
from Bio.SeqIO import parse
from kblast_cls_basedbase import BaseDataBase
from commons.warns import tell
#
#
class IterFasta:
    """Iterator class that returns records from a sequence database.

    Available Bio.SeqIO parser types:
        fasta      Resulting sequences have a generic alphabet
        genbank    Uses Bio.GenBank
        embl       Also uses Bio.GenBank
        clustal    Currently uses Bio.ClustalW for writing
        swiss      Swiss-Prot aka UniProt. Uses Bio.SwissProt
        nexus      aka PAUP format. Uses Bio.Nexus
        phylip     Truncates names at 10 characters.
        stockholm  Also known as PFAM format.
        
    """
    def __init__(self, dbase_file, dbase_type):
        """Select parser and get iterator over the parsed file"""
        self.handle = open(dbase_file)
        self.dbase_type = dbase_type
        if self.dbase_type in ['uniprot1', 'uniprot', 'ncbi']:
            parser_type = "fasta"
        else:
            parser_type = None
        self.parser = parse(self.handle, parser_type)

    def __iter__(self):
        """iter"""
        return self

    def __next__(self):
        """next"""
        try:
            record = self.parser.__next__()
        except StopIteration:
            self.handle.close()
            raise StopIteration

        id_ = get_accession_num(record, self.dbase_type)
        desc = str(record.description)  # id + description
        seq = str(record.seq)

        return id_, desc, seq
#
#
class DataBase(BaseDataBase):
    """A dictionary class with data from a sqlite3 database.

    """
    def __init__(self, sqlbase):
        """Init Super.

        Attributes:
        sql_dbase : the sqlite file (dbase.idx)
        fasta_file : fasta original to fill de dbase
        dbase_type :  database type

        """
        BaseDataBase.__init__(self, sqlbase)

    # noinspection SqlDialectInspection,SqlNoDataSourceInspection
    def fill(self, fasta_file, dbase_type='uniprot'):
        """Fill and index database.

        fasta_file is the input fasta file

        """
        self.dbase_type = dbase_type
        try:
            self.cur.execute(
                """create table sqltable (id text, desc text, seq text)""")
        except sqlite3.OperationalError as data:
            tell(str(data))        # already exists
        else:
            iterator = IterFasta(fasta_file, self.dbase_type)
            self.cur.executemany(
                """insert into sqltable values (?, ?, ?)""", iterator)
            self.conn.commit()
            self.cur.execute("""create index idx on sqltable(id)""")
            self.conn.commit()
#
#
def get_accession_num(fasta_record, record_type="uniprot"):
    """Get accession number from a fasta record.

    Used by IterFasta class, yields the id for indexation of the sqlite dbase.
    That is, it produces the keys for the dictionary sqlite(file *.idx) created
    in the indexation process.
    This key is the same that has to be looked for in headers in functions
    like <get_title_components(alignment, dictionary, dbase_type='ncbi')>
    when we want to extract data from the sqlite dictionary.

    NCBI      >gi|6273290|gb|AF191664.1|AF191664 Opuntia clavata...
    Uniprot1  >Q4U9M9|104K_THEAN 104 kDa micro/rop_ry pre - Theileria an...
    Uniprot   >sp|Q4U9M9|104K_THEAN 104 kDa micro/rop_ry pre - Theileria an...
    Note:
    swissprot >gi|21542352|sp||P30930_1 [Segment 1 of 11] Phosphatidylcholine...
    """
    accession_atoms = fasta_record.id.split('|')
    #
    if record_type == "uniprot1":
        acc_number = accession_atoms[0]
    elif record_type == "uniprot":
        acc_number = accession_atoms[1]
    elif record_type == "ncbi":
        acc_number = accession_atoms[3]
    else:
        acc_number = '000'

    return acc_number
#
#
if __name__ == "__main__":

    import time
    # sql_dbase = r"C:\uniprot_dbase"
    DIR = os.path.join('C:', os.sep, 'Blast', 'DBs')
    filein = os.path.join(DIR, "uniprot.fasta")
    sql_dbase = os.path.join(DIR, "human_ust.idx")

    dbase = DataBase(sql_dbase)
    time1 = time.time()
    # test iteration
    for item in dbase:
        pass
    time2 = time.time()
    # test reference
    result = dbase['Q9TQH5']
    print('- result')
    for item in result:
        print(item[:50])
    # give me five
    n = 0
    print("- five first")
    for item in dbase:
        if n > 4:
            break
        print(item[0])
        n += 1

    print('- execution time\n%1.5f sec' % (time2 - time1))

# - result
# Q9TQH5
# Q9TQH5|1A02_HUMAN HLA class I histocompatibility a
# MAVMAPRTLVLLLSGALALTQTWAGSHSMRYFFTSVSRPGRGEPRFIAVG
# - five first
# P31946
# P62258
# Q04917
# P61981
# P31947
# - execution time
# 0.15391 sec
