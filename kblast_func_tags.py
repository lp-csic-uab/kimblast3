#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
#
"""
kblast_func_tags
"""
#
# tags is a list of Tag objects with attributes:
#    self.id = rec_id   # rec_id = (sequence, file, tag_masses, is_series_head)
#    self.record = record
#    self.enzyme = enzyme
#    self.side_length = 20
#    self.prot_id1 =[]
#    self.prot_id2 =[]
#    self.names = []
#    self.prot_seq = []
#    self.diana = []
#    self.patron = []
#    self.subject = []
#    self.head_tail = {}           #no
#    self.head_tail_alpha = {}     #no
#    self.start = []
#    self.stop = []
#    self.offset = []
#    self.extra_coverage = []
#    self.score = []
#    self.rel_hit = []              #no
#
# attributes = ['prot_id1', 'prot_id2', 'names', 'prot_seq',
#               'diana', 'patron', 'subject', 'start', 'stop',
#                'offset', 'extra_coverage', 'score']
#
import re
from kblast_cls_tag import Tag
from kblast_funcs import parse_info_sequences, get_title_components
from kblast_funcs import pass_filter
from kblast_textos import HEADER, HEADER_HTML, XLS_HEAD, XLS_INI
from kblast_textos import TAG_LINE, XLS_END
#
codes = ('IL', 'J')
#
#
def join_tags(tags):
    """Combine hit collections from different mutants.

    original    blasted    combined
      ALS  -->  AIS, ALS --> AJS

    """
    new_tags = []
    start = True
    tag_parent = None

    for tag in tags:
        if tag.id[3] is True:             # is_series_head = True
            if start:
                start = False
            else:
                seq = tag_parent.id[0]
                replaced = False
                for aa in codes[0]:
                    if aa in seq:
                        seq = seq.replace(aa, codes[1])
                        replaced = True
                if replaced:
                    tag_parent.id = (seq, tag_parent.id[1],
                                     tag_parent.id[2], tag_parent.id[3])
                new_tags.append(tag_parent)
            #
            tag_parent = tag
            continue

        for item in Tag.attributes:
            # noinspection PyUnresolvedReferences
            getattr(tag_parent, item).extend(getattr(tag, item))

    seq = tag_parent.id[0]
    replaced = False
    for aa in codes[0]:
        if aa in seq:
            seq = seq.replace(aa, codes[1])
            replaced = True
    if replaced:
        tag_parent.id = (seq, tag_parent.id[1],
                         tag_parent.id[2], tag_parent.id[3])
    new_tags.append(tag_parent)

    return new_tags
#
#
def sort_tags(tags, num_max=5):
    """Short tag hits by score.

    For each identified protein the lower score hit is taken.
    100% identity <=> score = 0.
    Each protein is taken only once (its higher ranking hit).

    An alternative is to simply short all the tags by score or rel_hit.
    This could give two versions of the query pointing to the same protein.

    """

    new_tags = []
    for tag in tags:
        ids = tag.prot_id1
        scores = tag.score
        pos_to_keep = []

        # for each protein (each different item in tag.prot_id1)
        # the hit of higher score is taken
        for item in set(ids):
            number = ids.count(item)
            if number > 1:
                # all the matches of a given protein
                repeated = []
                pos = -1
                for inx in range(number):
                    pos = ids.index(item, pos + 1)
                    repeated.append(pos)

                low_score = 5000000
                best_pos = 0

                for pos in repeated:
                    score = scores[pos]
                    if score < low_score:
                        low_score = score
                        best_pos = pos
                pos_to_keep.append(best_pos)
            else:
                pos_to_keep.append(ids.index(item))

        # print(pos_to_keep)
        to_order = [(scores[pos], pos) for pos in pos_to_keep]
        to_order.sort()
        pos_to_keep = [pos for item, pos in to_order]
        #
        for item in Tag.attributes:
            if not pos_to_keep:
                break
            new_list = []
            old_list = list(getattr(tag, item))
            #
            for number, pos in enumerate(pos_to_keep):
                if number > num_max - 1:
                    break
                new_list.append(old_list[pos])
            setattr(tag, item, new_list)

        new_tags.append(tag)

    return new_tags
#
#
def build_tag_list(sequences, records, dictionary,
                   file_type='uniprot', afilter=None, enzyme='trypsin'):
    """Produces a list of Tag instances.

    sequences: list of queried sequences.
    records: list of blast result records for the queries.
             Only queries with hits produce a record. xml
    dictionary: file fasta.idx
    file_type: indicates how to get the correct key for <dictionary>, as
              uniprot is different from ncbi

    Initiates Tag instances from:
    -seq_info_list ->
        list of tuples: (query sequence, info_file, mass_data, is_series_head)
    -dictionary title components
        tuples: (prot_id1, prot_id2, names, prot_seq)
    -hsps from record.alignments.hsps

    Returns a list of Tag instances

    """
    #
    seq_info_list = parse_info_sequences(sequences)
    #
    tags = []
    #
    for record, seq_info in zip(records, seq_info_list):
        tag = Tag(seq_info, enzyme)
        for alignment in record.alignments:
            if afilter and pass_filter(alignment, afilter):
                continue
            prot_id1, prot_id2, names, prot_seq = \
                get_title_components(alignment, dictionary, file_type)
            # in cases where id is not taken correctly.
            # better fix it in get_accession_num/get_title_components,
            # to no generate entries with problematic id in the first place.
            if len(prot_id1) < 3:
                continue
            #
            for hsp in alignment.hsps:
                tag.set_title_components(prot_id1, prot_id2, names, prot_seq)
                tag.set_tag(hsp, prot_seq)

        tags.append(tag)

    return tags
#
#
def get_html_from_tags(tags, method='get_colored'):
    """"""
    sequence_data = []

    FORM = ('%-14s\t'       # linkcode
            '%-40s\t'       # match
            '%8s'           # mass
            '%6s'           # hits
            '%8s'           # score
            '%6s'           # start
            '%6s    '       # stop
            '%s')           # protein name

    FORM1 = FORM
    pattern = '<.*?>'
    header = FORM % HEADER_HTML
    first_tag = True
    for tag in tags:
        get_sec_html = getattr(tag, method)
        sequence_data.append('')
        sequence_data.append(tag.get_header())
        first_alignment = True
        for index in range(len(tag.prot_id1)):
            # noinspection PyCallingNonCallable
            seq_html = get_sec_html(index)
            if first_alignment:
                first_alignment = False
                len_fst_seq = len(re.sub(pattern, '', seq_html))
                FORM1 = FORM.replace('-40s', '-%is' % len_fst_seq)
                if first_tag:
                    first_tag = False
                    header = FORM1 % HEADER_HTML
            #
            txt = FORM1 % (tag.get_link(index),           # linkcode
                           seq_html,                      # sec_html
                           '%.2f' % tag.get_mass(index),  # mass
                           tag.get_hit_percent(index),    # rel_hits,
                           '%6.2f' % tag.score[index],    # score,
                           tag.start[index],              # start,
                           tag.stop[index],               # stop,
                           tag.names[index][0])           # name)
            #
            sequence_data.append(txt)

    sequence_data.insert(0, header)

    return '\n'.join(sequence_data)
#
#
def get_xls_from_tags(tags, method='get_colored'):
    """"""
    chunks = [XLS_HEAD, XLS_INI, TAG_LINE % HEADER]

    for tag in tags:
        query = tag.id[0]
        file_origin = tag.id[1]
        get_sec_xls = getattr(tag, method)
        if not len(tag.prot_id1):
            txt = TAG_LINE % (query, file_origin,
                              '*', '*', '*',
                              '*', '*', '*', '*', '*', '*')
            chunks.append(txt)
            continue
        #
        for index in range(len(tag.prot_id1)):
            # noinspection PyCallingNonCallable
            txt = TAG_LINE % (query, file_origin,
                              tag.prot_id1[index],           # prot_id1
                              tag.prot_id2[index],           # prot_id2
                              get_sec_xls(index),            # sec_xls
                              '%.2f' % tag.get_mass(index),  # mass
                              tag.get_hit_percent(index),    # rel_hits
                              '%.3f' % tag.score[index],     # score
                              tag.start[index],              # start
                              tag.stop[index],               # stop
                              tag.names[index][0]            # name
                              )
            chunks.append(txt)

    chunks.append(XLS_END)
    excel = ''.join(chunks)
    return excel
