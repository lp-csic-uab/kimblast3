#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
kblast_texts
"""
#
XLS_HEAD = """<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 9">
<link rel=File-List href="./Salida_archivos/filelist.xml">
<style id="test_3721_Styles">
<!--table
    {mso-displayed-decimal-separator:"\,";
    mso-displayed-thousand-separator:"\.";}
.font53
    {color:windowtext;
    font-size:8.0pt;
    font-weight:400;
    font-style:normal;
    text-decoration:none;
    font-family:Courier, monospace;
    mso-font-charset:0;}
.font63
    {color:red;
    font-size:8.0pt;
    font-weight:700;
    font-style:normal;
    text-decoration:none;
    font-family:Courier, monospace;
    mso-font-charset:0;}
.font73
    {color:blue;
    font-size:8.0pt;
    font-weight:700;
    font-style:normal;
    text-decoration:none;
    font-family:Courier, monospace;
    mso-font-charset:0;}
.font83
    {color:yellow;
    font-size:8.0pt;
    font-weight:700;
    font-style:normal;
    text-decoration:none;
    font-family:Courier, monospace;
    mso-font-charset:0;}
.font93
    {color:green;
    font-size:8.0pt;
    font-weight:700;
    font-style:normal;
    text-decoration:none;
    font-family:Courier, monospace;
    mso-font-charset:0;}
.xl24
    {padding-top:1px;
    padding-right:1px;
    padding-left:1px;
    mso-ignore:padding;
    color:windowtext;
    font-size:8.0pt;
    font-weight:400;
    font-style:normal;
    text-decoration:none;
    font-family:Courier, monospace;
    mso-font-charset:0;
    mso-number-format:General;
    text-align:general;
    vertical-align:bottom;
    mso-background-source:auto;
    mso-pattern:auto;
    white-space:nowrap;}
.xl26
    {padding-top:1px;
    padding-right:1px;
    padding-left:1px;
    mso-ignore:padding;
    color:windowtext;
    font-size:8.0pt;
    font-weight:400;
    font-style:normal;
    text-decoration:none;
    font-family:Courier, monospace;
    mso-font-charset:0;
    mso-number-format:General;
    text-align:center;
    vertical-align:bottom;
    mso-background-source:auto;
    mso-pattern:auto;
    white-space:nowrap;}
-->
</style>
</head>
"""

TAG_LINE = """<tr class=xl24 height=17
  style='mso-height-source:userset;height:12.75pt'>
  <td height=17 class=xl24 width=117 style='height:12.75pt;width:88pt'>%s</td>
  <td class=xl24 width=123 style='width:92pt'>%s</td>
  <td class=xl24 width=90 style='width:68pt'>%s</td>
  <td class=xl24 width=90 style='width:68pt'>%s</td>
  <td class=xl26 width=446 style='width:335pt'>%s</td>
  <td class=xl24 align=right width=80 style='width:60pt' x:num>%s</td>
  <td class=xl24 align=right width=80 style='width:60pt' x:num>%s</td>
  <td class=xl24 align=right width=80 style='width:60pt' x:num>%s</td>
  <td class=xl24 align=right width=80 style='width:60pt' x:num>%s</td>
  <td class=xl24 align=right width=80 style='width:60pt' x:num>%s</td>
  <td class=xl24 width=659 style='width:494pt'>%s</td>
 </tr>
"""

XLS_INI = """<body>
<!--[if !excel]>&nbsp;&nbsp;<![endif]-->
<!--The following info was generated by the tool Publish as Web page of
Microsoft Excel.-->
<!--If the same element is published again from Excel, all the
information between DIV tags will be replaced.-->
<!----------------------------->
<!--START DE LOS RESULTS DEL ASISTANTE TO PUBLISH AS EXCEL WEB PAGE -->
<!----------------------------->

<div id="test_3721" align=center x:publishsource="Excel">

<table x:str border=0 cellpadding=0 cellspacing=0 width=1755
 style='border-collapse:collapse;table-layout:fixed;width:1317pt'>
 <col class=xl24 width=117 style='mso-width-source:userset;mso-width-alt:
 4278;width:88pt'>
 <col class=xl24 width=123 style='mso-width-source:userset;mso-width-alt:
 4498;width:92pt'>
 <col class=xl24 width=90 style='mso-width-source:userset;mso-width-alt:
 3291;width:68pt'>
 <col class=xl24 width=90 style='mso-width-source:userset;mso-width-alt:
 3291;width:68pt'>
 <col class=xl26 width=446 style='mso-width-source:userset;mso-width-alt:
 16310;width:335pt'>
 <col class=xl24 width=80 span=5 style='width:60pt'>
 <col class=xl24 width=659 style='mso-width-source:userset;mso-width-alt:
 24100;width:494pt'>
"""

XLS_END = """</table>
</body>
</html>"""

HEADER = ('QUERY', 'FILE', 'ACCESSION', 'ENTRY NAME', 'BLAST MATCH',
          'MASS', '%HITS', 'SCORE', 'START', 'STOP', 'NAME')
HEADER_HTML = ('ACCESSION', 'BLAST MATCH', 'MASS',
               '%HITS', 'SCORE', 'START', 'STOP', 'NAME')

ABOUT_TXT = ["If you read this is because the 'about' document",
             "with general instructions for this program doesnt exist.",
             " work is in progress"]
#
#
if __name__ == '__main__':
    chunks = []
    query = 'ASDFGH'
    file_origin = 'fileA start#'
    ref_id = 'P4567'
    ref_id2 = 'JOJO_HUMAN'
    colored = '.....ASDFGH.....'
    mass = '1123.2'
    rel_hits = '99'
    score = '12.3'
    start = '123'
    stop = '223'
    name = 'protein B'

    chunks.append(XLS_HEAD)
    chunks.append(XLS_INI)
    txt = TAG_LINE % (query, file_origin, ref_id, ref_id2, colored, mass,
                      rel_hits, score, start, stop, name)
    chunks.append(txt)

    chunks.append(XLS_END)
    excel = ''.join(chunks)

    print('********* Test Output at C:\test2.htm **********\n\n')
    print(excel)

    with open(r'C:\test2.htm', 'w') as f:
        f.write(excel)
