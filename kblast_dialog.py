#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
#
"""
kblast_dialog
"""
#
import os
import wx
from wx.html import HtmlWindow
# noinspection PyProtectedMember
from commons.warns import is_ok
from kblast_textos import ABOUT_TXT
#
#
# noinspection PyUnusedLocal,PyArgumentList
class Colors(wx.MiniFrame):
    """Frame with color keys"""
    exists = False
    def __init__(self, pos=(800, 100), size=(180, 180)):
        wx.MiniFrame.__init__(self, None, -1, "COLOR CODES", pos, size,
                              style=wx.DEFAULT_FRAME_STYLE)
        #
        Colors.exists = True
        self.SetBackgroundColour('white')
        #
        bold = wx.Font(8,
                       wx.FONTFAMILY_DEFAULT,
                       wx.FONTSTYLE_NORMAL,
                       wx.FONTWEIGHT_BOLD, 0, "")

        normal = wx.Font(8,
                         wx.FONTFAMILY_DEFAULT,
                         wx.FONTSTYLE_NORMAL,
                         wx.FONTWEIGHT_BOLD, 0, "")

        orange = wx.Colour(255, 196, 47)

        colors = (
            ('LABORAT',     orange,  bold,  '  no match'),
            ('PROTEIN',     'red',   bold,  '  full match'),
            ('AMINOAC',     'blue',  bold,  '  conservative replace'),
            ('JOAQUIN',     'green', bold,  '  AAs excluded in Blast query'),
            ('MA- - -AR',   'black', bold,  '  gap in protein  '),
            ('CSICUAB',     'black', bold,  '  gap in query  '),
            ('I N P E P T', 'black', normal, '  off-match AAs in-digest'),
            ('. . . . . . .', 'black', normal, '  protein AAs off-digest'),
            ('- - - - - - -', 'black', normal, '  off protein')
        )
        #
        sizer = wx.GridBagSizer(0, 0)
        sizerv = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(5, 5, wx.GBPosition(0, 0))
        for nmr, color in enumerate(colors):
            chk = wx.StaticText(self, -1, color[0])
            chk.SetForegroundColour(color[1])
            chk.SetFont(color[2])
            sizer.Add(chk, pos=(nmr + 1, 1))
            chk = wx.StaticText(self, -1, color[3])
            sizer.Add(chk, pos=(nmr + 1, 3))
        # noinspection PyUnboundLocalVariable
        sizer.Add(5, 10, wx.GBPosition(nmr + 2, 4))

        self.bt_ok = wx.Button(self, -1, 'OK', size=(150, 20))
        #
        self.Bind(wx.EVT_BUTTON, self.on_close, self.bt_ok)
        self.Bind(wx.EVT_CLOSE, self.on_close)
        #
        sizerv.Add(sizer, 1)
        sizerv.Add(self.bt_ok, 0, wx.EXPAND)
        #
        self.SetSizer(sizerv)
        self.Fit()
        self.SetMinSize(size)

    def on_close(self, evt):
        Colors.exists = False
        self.Destroy()
#
#
# noinspection PyArgumentList,PyUnusedLocal
class MyHtmlWindow(HtmlWindow):
    def __init__(self, *args, **kwargs):
        HtmlWindow.__init__(self, *args, **kwargs)

    # noinspection PyMethodOverriding
    def OnLinkClicked(self, link_info):
        url = link_info.GetHref()
        url_protocol = url.partition(':')[0]
        if url_protocol in ('http', 'https', 'ftp', 'sftp'):
            import webbrowser
            webbrowser.open(url)
        else:
            super(MyHtmlWindow, self).OnLinkClicked(link_info)
#
#
# noinspection PyArgumentList,PyUnusedLocal
class About(wx.MiniFrame):
    """Frame with the About text"""
    exists = False
    def __init__(self, type_='html', pos=(100, 100)):
        wx.MiniFrame.__init__(self, None, -1, 'About KimBlast', pos,
                              style=wx.DEFAULT_FRAME_STYLE)
        #
        About.exists = True
        minsize = 520, 300
        if type_ == 'text':
            startsize = 520, 700
            #
            try:
                text = open('README.txt').read()
            except IOError:
                text = '\n'.join(ABOUT_TXT)
            #
            self.tc_readme = wx.TextCtrl(self, -1, text,
                                         style=wx.TE_MULTILINE | wx.TE_READONLY)
        else:
            startsize = 800, 700
            #
            self.tc_readme = MyHtmlWindow(self, -1)
            url = "doc/README.html"
            self.tc_readme.LoadFile(url)

        self.bt_close = wx.Button(self, -1, 'OK', size=(-1, -1))
        #
        self.Bind(wx.EVT_BUTTON, self.on_close, self.bt_close)
        self.Bind(wx.EVT_CLOSE, self.on_close)
        #
        sizerv = wx.BoxSizer(wx.VERTICAL)
        sizerv.Add(self.tc_readme, 1, wx.EXPAND)
        sizerv.Add(self.bt_close, 0, wx.EXPAND)
        #
        self.SetSizer(sizerv)
        self.Fit()
        self.SetMinSize(minsize)
        self.SetSize(startsize)

    def on_close(self, evt):
        About.exists = False
        self.Destroy()
#
#
def open_file(win=None, def_dir=None, must_exist=False):
    """Dialog for file opening"""
    fpath = None
    file_exists = False
    #
    try:
        current_file = win.GetValue()
    except AttributeError:
        current_file = None
    #
    if current_file:
        default_dir = current_file
        default_file, _ = os.path.splitext(os.path.basename(current_file))
    else:
        default_dir = def_dir if def_dir else os.getcwd()
        default_file = ""
    #
    fd = wx.FileDialog(None, defaultDir=default_dir, defaultFile=default_file)
    #
    if fd.ShowModal() == wx.ID_OK:
        fpath = fd.GetPath()
        try:
            win.SetValue(fpath)
        except AttributeError:
            pass
        #
        if os.path.exists(fpath):
            file_exists = True
        elif must_exist:
            if is_ok('File does not exists! Do you want to create it?'):
                open(fpath, 'w').write('*')
                file_exists = True
    #
    fd.Destroy()
    return fpath, file_exists
#
#
#
if __name__ == '__main__':
    app = wx.App()
    Colors().Show()
    About().Show()
    app.MainLoop()
