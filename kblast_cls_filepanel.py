# -*- coding: UTF-8 -*-
#
# generated by wxGlade 0.9.9pre on Sat Apr 25 14:49:56 2020
#

import wx


class FilePanel(wx.Panel):
    def __init__(self, parent, dir_in, dir_out, ID=-1,
                 pos=wx.DefaultPosition, size=(200, 60)):
        wx.Panel.__init__(self, parent, ID, pos, size, wx.RAISED_BORDER)

        self.SetBackgroundColour("white")
        self.SetMinSize(size)
        self.schoices = [('oxidant & ((lipase and protein) OR '
                          '(phospho y (fragment o precursor)))'),
                         'haptoglobin']
        #
        EXPRESSION = ('Expression to be searched in current fasta database'
                      '(Search db)\nor for filtering Blast results (text '
                      'filter)\n\nCan contain logical options:\n'
                      '   AND (accepts: AND, and, y, Y, & )\n'
                      '   OR   (accepts: OR, or, o, O, | )\n'
                      '   NOT (accepts: not, NOT, no, ! )'
                      )
        
        FLAG = wx.ALIGN_CENTER_VERTICAL

        st_fin = wx.StaticText(self, size=(60, 20), label="Seq File")
        self.tc_fin = wx.TextCtrl(self, wx.ID_ANY, style=wx.TE_READONLY)
        self.bt_fin = wx.Button(self, size=(35, 20), label="···")

        st_fout = wx.StaticText(self, size=(60, 20), label="FileOut")
        self.tc_fout = wx.TextCtrl(self, wx.ID_ANY, style=wx.TE_READONLY)
        self.bt_fout = wx.Button(self, size=(35, 20), label="···")
        #
        self.ck_batch = wx.CheckBox(self, wx.ID_ANY, "is bacht dir")
        self.bt_inc = wx.Button(self, size=(35, 15), label=">")
        self.bt_dec = wx.Button(self, size=(35, 15), label="<")
        #
        st_search = wx.StaticText(self, size=(80, 20), label="Text Search")
        self.tc_search = wx.ComboBox(self, size=(-1, 20), choices=self.schoices,
                                     style=wx.CB_DROPDOWN)
        self.bt_search = wx.Button(self, size=(80, 20), label="Search Db")
        #
        self.tc_fin.SetToolTip("file with sequences to be blasted")
        self.tc_fout.SetToolTip("file to save blast results")
        self.ck_batch.SetToolTip("blast all files in the directory")
        self.bt_inc.SetToolTip("show more")
        self.bt_dec.SetToolTip("show less")
        self.tc_search.SetToolTip(EXPRESSION)
        self.bt_search.SetToolTip('Search expression in current fasta db')
        #
        self.tc_fin.SetMinSize((250, 20))
        self.tc_fout.SetMinSize((250, 20))
        #
        self.Bind(wx.EVT_BUTTON, self.on_search, self.bt_search)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_search, self.tc_search)
        #
        szr_1 = wx.BoxSizer(wx.HORIZONTAL)
        szr_2 = wx.BoxSizer(wx.VERTICAL)
        szr_3 = wx.BoxSizer(wx.VERTICAL)
        szr_4 = wx.BoxSizer(wx.HORIZONTAL)
        szr_5 = wx.BoxSizer(wx.HORIZONTAL)
        szr_6 = wx.BoxSizer(wx.HORIZONTAL)
        szr_7 = wx.BoxSizer(wx.HORIZONTAL)
        szr_8 = wx.BoxSizer(wx.HORIZONTAL)
        #
        szr_1.Add(szr_2, 1, 0, 0)
        szr_2.Add(szr_4, 1, wx.BOTTOM | wx.EXPAND | wx.TOP, 2)
        szr_4.Add(st_fin, 0, FLAG, 0)
        szr_4.Add(self.tc_fin, 1, 0, 0)
        szr_4.Add(self.bt_fin, 0, 0, 0)
        szr_2.Add(szr_5, 1, wx.BOTTOM | wx.EXPAND | wx.TOP, 2)
        szr_5.Add(st_fout, 0, FLAG, 0)
        szr_5.Add(self.tc_fout, 1, 0, 0)
        szr_5.Add(self.bt_fout, 0, 0, 0)
        szr_1.Add(szr_3, 1, 0, 0)
        szr_3.Add(szr_6, 1, wx.BOTTOM | wx.EXPAND | wx.TOP, 2)
        szr_6.Add(self.ck_batch, 0, FLAG | wx.LEFT, 3)
        szr_6.Add((20, 20), 1, FLAG, 0)
        szr_6.Add(szr_8, 0, wx.EXPAND, 0)
        szr_8.Add(self.bt_inc, 0, FLAG, 0)
        szr_8.Add(self.bt_dec, 0, FLAG, 0)
        szr_3.Add(szr_7, 1, wx.BOTTOM | wx.EXPAND | wx.TOP, 2)
        szr_7.Add(st_search, 0, FLAG | wx.LEFT, 10)
        szr_7.Add(self.tc_search, 1, 0, 0)
        szr_7.Add(self.bt_search, 0, FLAG, 0)
        #
        self.SetSizer(szr_1)
        self.Fit()
        #
        self.tc_fin.SetValue(dir_in)
        self.tc_fout.SetValue(dir_out)

    def on_search(self, evt):
        """Controls filter text input and adds new entries to the list"""
        text = self.tc_search.GetValue()
        if text == '':
            self.tc_search.SetValue(text)
            return
        if text not in self.schoices:
            self.schoices.append(text)
            self.tc_search.SetItems(self.schoices)
            self.tc_search.SetValue(text)
        evt.Skip()
        

if __name__ == '__main__':

    class TestFrame(wx.Frame):
        def __init__(self, pos=(100,100)):
            """"""
            wx.Frame.__init__(self, None, -1, 'About KimBlast', pos,
                              style=wx.DEFAULT_FRAME_STYLE)
            self.pn = FilePanel(self, dir_in='C:\\', dir_out='C:\\')
            szr = wx.BoxSizer(wx.VERTICAL)
            szr.Add(self.pn, 0, wx.EXPAND, 0)
            self.SetSizer(szr)
            self.Layout()

    app = wx.App()
    TestFrame().Show()
    app.MainLoop()
