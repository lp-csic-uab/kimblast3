# -*- mode: python ; coding: utf-8 -*-

import configparser
import shutil

config = configparser.ConfigParser()
config.read_file(open('install.ini'))

app_name = config.get('Common', 'name')

apps = ['bsddb', 'curses', 'pywin.debugger',
        'pywin.debugger.dbgcon', 'pywin.dialogs',
        'tcl', 'tkconstants', 'tkinter', 'pygtk',
        'pydoc', 'doctest', 'test',
        'bsddb', 'curses', 'themail', '_fltkagg',
        '_gtk', '_gtkcairo', '_agg2', '_gtkagg',
        '_tkagg', '_cairo', 'numpy', 'matplotlib',
        ]
        
dlls = ['libgdk-win32-2.0-0.dll',
        'libgobject-2.0-0.dll',
        'tcl86.dll', 'tk86.dll',
        'libgdk_pixbuf-2.0-0.dll',
        'libgtk-win32-2.0-0.dll',
        'libglib-2.0-0.dll',
        'libcairo-2.dll',
        'libpango-1.0-0.dll',
        'libpangowin32-1.0-0.dll',
        'libpangocairo-1.0-0.dll',
        'libglade-2.0-0.dll',
        'libgmodule-2.0-0.dll',
        'libgthread-2.0-0.dll',
        'QtGui4.dll', 'QtCore.dll',
        'QtCore4.dll'
         ]
                 
files = ['INSTALL.txt',
         'README.txt',
         'LICENCE.txt',  
         'config_init.cfg',
         'config_common_win.cfg']
        
folders = ['doc', 'data', 'params']
        
#####################################################
        
block_cipher = None                         
                         
a = Analysis(['kblast_main.pyw'],
             pathex=['P:\\Python38\\programas\\kimblast'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=apps,
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)

pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)


a.binaries = TOC([x for x in a.binaries if x[0] not in dlls])

exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name=app_name,
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False )

#######################################################


for path in files:
    shutil.copyfile(path, os.path.join('dist', path))
    
for folder in folders:
    shutil.copytree(folder, os.path.join('dist', folder))
