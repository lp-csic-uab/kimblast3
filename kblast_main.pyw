#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
#
"""
KBLAST_MAIN
"""
#
import os
import wx
import re
import json
import urllib
import urllib.parse
import urllib.request
import kblast_common as kbc
from urllib.error import URLError
from collections import Counter
from tempfile import mkstemp
from commons.warns import tell
from commons.mcalc import get_mass
from kblast_cls_setupframe import SetupFrame
from kblast_cls_dbase import DataBase
from kblast_cls_baseframe import BaseFrame, MyParams
from kblast_funcs import blast, save_blast_output, record_from_xhandle
from kblast_funcs import readfile, it_is_exe
from kblast_funcs import search_proteins
from kblast_funcs import digest, Enzyme
from kblast_combinator import mutate_entries
from kblast_func_tags import join_tags, sort_tags, get_xls_from_tags
from kblast_func_tags import build_tag_list, get_html_from_tags
#
FASTA = 0
FORMAT_FILE = 1
INDEX_FILE = 2
FASTA_TYPE = 3      # protein/nucleotide
FASTA_FORMAT = 4    # uniprot1/uniprot/ncbi
#
COMPILED = re.compile("(\[(\d+)\])*([A-Z]*)")
#
#
# name_list -->fasta_original--FORMAT_FILE--INDEX_FILE--fasta_type--fasta_format
DEFAULT_DTBS = {'test': ('test.fasta',
                         'test.psq', 'test.idx',
                         'protein',
                         'uniprot')
                }
#
# Fix makeblastdb 2.10.0 error:
# 'Error: mdb_env_open: There is not enough space on the disk.'
# https://www.biostars.org/p/413294/
if os.name == 'nt':
    os.environ['BLASTDB_LMDB_MAP_SIZE'] = '1000000'
#
#
class BlastFrame(BaseFrame):
    """mainframe"""
    def __init__(self, title, pos=wx.DefaultPosition):
        """"""
        self.dictionary = {}    # sqlite3 dictionary class
        self.dict_dtbs = {}     # dbase file data from config.txt
        self.tags = []
        self.enzyme = ''
        self.dbase = ''
        self.custom_method = ''
        self.view_format = ''
        self.web_query = ''
        self.has_changed = False
        self.html_size = 1
        self.html = ''
        self.current_path = ''
        self.user_ip = None
        self.api_key = None
        self.score = None
        self.species = None
        self.setup_frame = None
        self.DIRQUIM = kbc.config.get('directories', 'DIRQUIM')
        self.BLASTPATH = kbc.config.get('directories', 'BLASTPATH')
        self.DBPATH = kbc.config.get('directories', 'DBPATH')
        self.SAVEXCEL = kbc.config.get('directories', 'SAVEXCEL')
        self.BATCHFILE = kbc.config.get('directories', 'BATCHFILE')
        self.init_all()
        self.check_blast_dbs()
        #
        BaseFrame.__init__(self, title=title, pos=pos)
        #
        if os.name == 'posix':
            f = self.GetFont()
            f.SetPointSize(9)
            self.SetFont(f)
        #
        self.ps_cbboxes = self.pn_param.wleft.param_selector_cbboxes
        #
        calc = self.pn_param.wcenter.calc
        calc.bt_calc.SetToolTip(
            'Press to calculate mass of selected sequence')
        tc_string = ('                Peptide Mr Calculator\n'
                     '           Write a sequence or highlight it\n'
                     '            in navigator and press calc')
        calc.tc.SetValue(tc_string)
        calc.tc.SetToolTip(tc_string.replace('  ', ''))
    #
    def init_all(self):
        """Initialize dbases"""
        self.configure_dbases()
        self.init_dbase_choices()
    #
    def configure_dbases(self):
        """Read config and fills self.dict_dtbs.

        If config is not found uses data in DEFAULT_DTBS.

        """
        try:
            handle = open(kbc.config.get('directories', 'DB_CONF_FILE'), 'r')
        except IOError:
            self.dict_dtbs.update(DEFAULT_DTBS)
            save_dbase_conf(self.dict_dtbs)
            return
        #
        # test: test.fasta, test.psq, test.idx, protein, uniprot
        for line in handle:
            name, file_data = line.split(':')
            files = tuple([item.strip() for item in file_data.split(',')])
            self.dict_dtbs[name] = files
        handle.close()
    #
    def init_dbase_choices(self):
        """Fills listing of_dbase_fnames with database given names.
        """
        if self.dict_dtbs:
            start = True
            for name in self.dict_dtbs:
                if start:
                    MyParams.dbase_fnames.append(name)
                    start = False
                MyParams.dbase_fnames.append(name)   # the first one, twice
        else:
            MyParams.dbase_fnames.append('no Dbase')
    #
    def check_blast_dbs(self):
        """ Warns about the lack of Blast and/or fasta folders.
         This is to assure the installation has been completed correctly
        """
        if not it_is_exe(os.path.join(self.BLASTPATH, 'blastp')):
            tell('You need to install Blast in %s'  % self.BLASTPATH,
                 'CAN NOT FIND BLAST')
       
        if not os.path.exists(self.DBPATH):
            msg = '''
                  You need to locate a fasta file in %s
                  and FORMAT it using the "Setup Fasta" utility
                  ''' % self.DBPATH
            
            tell(msg, 'CAN NOT FIND FASTA DATABASES')
    #
    def load_dictionary(self, dbase):
        """Fills self.dictionary with sequence records.

        self.dictionary is a DataBase instance filled with data
        from a *.idx file.

        """

        index_file = self.dict_dtbs[dbase][INDEX_FILE]
        index_path = os.path.join(self.DBPATH, index_file)
        #
        if os.path.isfile(index_path) and index_path.endswith('idx'):
            self.dictionary = DataBase(index_path)
        else:
            tell("The %s file is not a .idx file or was not found in %s.\n"
                 "You must create it using the 'Setup Fasta' utility"
                 % (index_file, self.DBPATH))
            self.dictionary = None
    #
    def on_search(self, evt):
        """Search proteins which contain the query in its database title.
        """
        query = self.pn_file.tc_search.GetValue()
        dbase = self.ps_cbboxes['dbase'].GetValue()
        dbase_path = os.path.join(self.DBPATH, self.dict_dtbs[dbase][FASTA])
        file_type = self.dict_dtbs[dbase][FASTA_FORMAT]

        if self.dbase != dbase:
            self.load_dictionary(dbase)
            if self.dictionary is None:
                return
            self.dbase = dbase
        #
        protein_list = search_proteins(query, dbase_path)
        html = get_html_from_search(query, protein_list, file_type)
        self.show_results(html, 'search')
    #
    def on_save_xls(self, evt):
        """Save a blast search result in excel format.
        """
        if self.custom_method:
            excel = get_xls_from_tags(self.tags, self.custom_method)
            try:
                open(self.SAVEXCEL, 'w').write(excel)
                if os.name == 'nt':
                    os.system('start EXCEL.EXE %s' % self.SAVEXCEL)
                elif os.name == 'posix':
                    os.system('soffice --calc -o ' + self.SAVEXCEL)  
            except IOError:
                tell('Can not create or open %s' % self.SAVEXCEL)
        else:
            tell(('                   Nothing to save.\n'
                  'Only Blast Results in Custom Formats are imported'))
    #
    def on_search_google(self, evt):
        """Search in google a text selected in the html window.

        The method makes consecutive searches for a keyword in a list of sites.
        Sites are read from config file (expasy and wikipedia).

        The Google AJAX API is not working anymore. Search is performed by the
        Google Custom Search Engine (CSE) API.  The CSE is a free service which
        allows 100 searchs per day. This requires:

        - A Google API key
        API keys are obtained in the Credentials page of the Google API console.
        (https://developers.google.com/api-client-library/python/guide/aaa_apikeys)

        - A Custom Search Engine ID
        The Search engine must be created in (https://cse.google.com/cse/all)

        See StackOverFlow Question 37083058 for more details
        (https://stackoverflow.com/questions/37083058/programmatically
        -searching-google-in-python-using-custom-search)

        ****************************************************************
        The API key and the CSE ID are read from the configuration file.
        ***********  Keep these keys OFF the repository ****************
        ****************************************************************

        """
        txt = None
        get = kbc.config.get
        selection = self.pn_html.web_view.GetSelectedText()
        if selection:
            txt = selection.strip('\t_,.* ')
        #
        if txt:
            self.web_query = txt
        if not self.web_query:
            tell('No query.\n Highlight a string in the KimBlast navigator')
            return
        #
        query = {}
        user_ip = get('google', 'USER_IP')
        api_key = get('google', 'API_KEY')
        #
        if not (user_ip and api_key):
            tell('Google Custom Search requires and API key and a Engine ID\n'
                 'See stackoverflow question 37083058')
            return
        #
        url_form = get('google', 'URL')

        sites_result = []
        sites = get('google', 'SITES').split('|')
        for site in sites:
            query_string = '%s site:%s' % (self.web_query, site)
            query.update({'q': query_string})
            g_query = urllib.parse.urlencode(query)
            url = url_form % (api_key, user_ip, 1, g_query)
            #
            try:
                request = urllib.request.Request(url)
            except URLError as err:
                log = "Error %s in Google Search for %s in site %s"
                log = log % (err, self.web_query, site)
                self.log_text(log, True)
                return
            #
            json_object = json.load(urllib.request.urlopen(request))
            #
            try:
                sites_result.append(json_object['items'])
            except KeyError:
                continue
        #
        chain = []
        link = '<b><a href=%s>%s</a></b>\t%s'
        for results in sites_result:
            for item in results:
                url = item['link']
                short_url = url.split('/')[2]
                summary = item['htmlSnippet']
                summary = summary.replace('\n', '').replace('<br>', '')
                linea = link % (url, short_url, summary)
                chain.append(linea)
        #
        text = '<br>\n'.join(chain)
        if text:
            self.show_results(text, 'google')
        else:
            log = "No Results in Google for %s" % self.web_query
            self.log_text(log, True)
    #
    def on_search_string(self, evt):
        """ search the STRING database for the interactors of a selected protein.

        Uses the URI:
        http://[database]/[access]/[format]/[request]?[parameter]=[value]
        To get the image of interactors an example call is:
        http://string-db.org/api/image/network?identifier=4932.YML115C&required_score=950&limit=10&network_flavor=evidence
        (http://version10.string-db.org/help/api/)

        """

        get = kbc.config.get
        selection = self.pn_html.web_view.GetSelectedText()
        if selection:
            text_blocks = selection.split()
            for text in text_blocks:
                if ('_' in text) and text.isupper():
                    species = text
                    break
            else:
                tell("No protein name (like 'XXXX_XXXX') in selection")
                return
        else:
            species = self.species if self.species else None
        #
        if self.species == species:
            self.score = self.score - 50
        else:
            self.score = 950
            self.species = species
        #
        url_form = get('string', 'URL')

        URL = url_form % (species, self.score)
        #
        log = '- Search STRING for species %s with score %i' % (species, self.score)
        self.pn_html.log_text(log)
        self.pn_html.location.SetValue(URL)
        self.pn_html.web_view.LoadURL(URL)
    #
    def get_clean_selection(self):
        """Clean selected text in the navigator of non amino acid characters.

        Remove spaces, tabs, dots, colons and asterisks.

        """
        selection = self.pn_html.web_view.GetSelectedText()
        return selection.strip('\t_,.* ')
    #
    def on_wider_frame(self, evt):
        """Frame is made one step wider.

        whatever --step--> 768 --step--> 855 --step--> 950
        """
        if self.IsMaximized():
            return
        #
        x, y = self.GetSize()
        if os.name == 'nt':
            self.move_size(y, x, 950, 3)
        else:
            self.move_size(y, x, 1025, 3)
    #
    def on_narrow_frame(self, evt):
        """Frame is made one step smaller

        If the frame is maximized it gets minimized, else
        Whatever size --step--> 950 --step--> 855  --step--> 768

        """
        if self.IsMaximized():
            self.Maximize(False)
            return
        #
        x, y = self.GetSize()
        if os.name == 'nt':
            self.move_size(y, x, 768, -3)
        else:
            self.move_size(y, x, 810, -3)
    #
    def move_size(self, y, start, stop, step):
        """"""
        for i in range(start, stop, step):
            self.SetSize((i, y))
        self.SetSize((stop, y))
    #
    def on_increase(self, evt):
        """Increases one point the font size in the html window.

        Valid sizes are -2 1 2 .... 7

        """
        if self.html_size == -2:
            size = 1
        else:
            size = min(self.html_size + 1, 7)

        self.change_size(size)
    #
    def on_decrease(self, evt):
        """Decreases one point the font size in the html window.

        Valid sizes are -2 1 2 .... 7

        """
        if self.html_size == 1:
            size = -2
        else:
            size = max(self.html_size - 1, -2)

        self.change_size(size)
    #
    def change_size(self, size):
        """Change WebView html window font size to <size>.

        Not necessary in windows because resize can be done with Ctrl+mouse.
        Required for Ubuntu wx-gtk.

        """
        txt = '<PRE><font size='
        location = self.pn_html.location.GetValue()
        # noinspection PyUnresolvedReferences
        location_protocol = location.partition(':')[0]
        if location_protocol in ('http', 'https', 'ftp', 'sftp'):
            return

        if self.html.startswith(txt):
            self.html_size = size
            pos = self.html.find('>', 8)
            self.html = '%s%i>%s' % (txt, self.html_size, self.html[pos + 1:])
            self.pn_html.web_view.SetPage(self.html, location)
    #
    def on_bt_calc(self, evt):
        """Reads selected sequence and sends it to the calculator
        """
        wc_mods = self.pn_param.wcenter.properties
        calc = self.pn_param.wcenter.calc
        #
        # very slow step in first reading
        sequence = self.get_clean_selection() or calc.tc.GetValue()
        #
        calc.clear()
        # eliminate numbers from the sequence string
        if sequence and not sequence.isalpha():
            text = [char for char in sequence if char.isalpha()]
            sequence = ''.join(text)
            sequence = sequence.upper()
            #
        if sequence:
            # 'carbamidomet', 'Met(O)', '-', '-'
            if wc_mods['C-cam'].IsChecked():
                sequence = sequence.replace('C', 'c')
            if wc_mods['M-ox'].IsChecked():
                sequence = sequence.replace('M', 'm')
            calc.calc(sequence)
    #
    def on_fasta_setup(self, evt):
        """Opens the Fasta setup frame"""
        if not SetupFrame.exists:
            self.setup_frame = SetupFrame(self, info_dtbs=self.dict_dtbs,
                                          db_path=self.DBPATH,
                                          blast_path=self.BLASTPATH,
                                          log=self.pn_html.log_text)
            self.setup_frame.Show()
        else:
            self.setup_frame.SetFocus()
    #
    def update_dictionary_choices(self):
        """Updates config.txt and databases in selector
        """
        choice_list = self.dict_dtbs.keys()
        self.pn_param.wleft.refresh_dbase_choices(choice_list,
                                                  default='human_uspt')
        #
        save_dbase_conf(self.dict_dtbs)
    #
    def get_custom_method(self):
        """"""
        if self.view_format == 'custom':
            method = 'get_colored'
        elif self.view_format == 'custom_bw':
            method = 'get_uncolored'
        else:
            method = ''

        return method
    #
    def on_blast(self, evt):
        """Run a blast search."""
        self.view_format = self.ps_cbboxes['format'].GetValue()
        self.custom_method = self.get_custom_method()      # colored / uncolored
        #
        self.tags = []
        dbase = self.ps_cbboxes['db'].GetValue()
        dbase_path = os.path.join(self.DBPATH,
                                  self.dict_dtbs[dbase][FORMAT_FILE])
        #
        if self.dbase != dbase:
            self.load_dictionary(dbase)
            if self.dictionary is None:
                return
            self.dbase = dbase
        #
        batch = self.pn_file.ck_batch.IsChecked()
        #
        if batch:
            # to prepare consecutive files
            dir_ = os.path.dirname(self.infile)
            path_list = [os.path.join(dir_, item) for item in os.listdir(dir_)]
            file_list = [item for item in path_list if os.path.isfile(item)]
        else:
            file_list = [self.infile]

        full = []
        is_first = True
        for afile in file_list:
            #
            sequences = readfile(afile)
            if not sequences:
                continue
            sequences = search_mass_info(sequences)
            #
            if self.pn_query.ck_mutate.IsChecked():
                sequences = mutate_entries(sequences)
            #
            self.pn_query.tc_query.Clear()
            self.pn_query.tc_query.WriteText(sequences)
            self.pn_query.bt_save.SetBackgroundColour('light grey')
            #
            fld, fname = mkstemp()
            with open(fname, 'w') as f:
                f.write(sequences)
            #
            # *****  BLAST CALL  *****#
            # xml file with the record
            result_file, error =\
                run_blast(self.ps_cbboxes, fname, self.outfile,
                          dbase_path, self.pn_html.log_text)
            #
            if error:
                self.log_text(error, True)
            #
            if self.custom_method:
                tags = self.get_tags_from_blast_result(fname, result_file)
                txt = get_html_from_tags(tags, self.custom_method)
                self.tags.extend(tags)
            else:
                txt = readfile(result_file)
            #
            if batch and self.custom_method:
                # noinspection PyUnresolvedReferences
                parts = txt.split('\n', 1)
                header = parts[0] if is_first else ''
                is_first = False
                txt = header + '\n' + afile + parts[1]

            full.append(txt)

        txt = '\n'.join(full)
        if not txt:
            tell('No sequences to Blast\n'
                 'Either:\nA) Load a file with sequences\n'
                 'B) Enter manually and Save one or more sequences')
        open(self.BATCHFILE, 'w').write(txt)
        self.show_results(txt, self.view_format)
    #
    def get_tags_from_blast_result(self, seq_file, blast_file):
        """Returns the contents of a blast search output file

        seq_file: name of file with the sequences blasted
        blast_file: file with Blast raw results (xml)

        """
        #
        dbase = self.ps_cbboxes['db'].GetValue()
        enzyme = self.ps_cbboxes['enzyme'].GetValue()
        mutate = self.pn_query.ck_mutate.IsChecked()
        merge_tags = self.pn_query.ck_join.IsChecked()
        #
        if self.pn_query.ck_filter.IsChecked():
            filter_ = self.pn_file.tc_search.GetValue()
        else:
            filter_ = None
        #
        file_type = self.dict_dtbs[dbase][FASTA_FORMAT]
        #
        with open(blast_file, 'r') as xhandle:
            records = record_from_xhandle(xhandle)
        #
        sequences = readfile(seq_file)
        #
        tags = build_tag_list(sequences, records, self.dictionary,
                              file_type, filter_, enzyme)
        #
        if merge_tags and mutate:
            max_number = self.ps_cbboxes['num_descriptions'].GetValue()
            tags = join_tags(tags)
            tags = sort_tags(tags, int(max_number))
        #
        return tags
    #
    def on_prot_select(self, url):
        """Shows info related with protein with accession <url>.

        Extracts a record from the database dictionary when a href is
        clicked in the HTML page of search results and shows:
        - protein references and names
        - sequence with digestion sites and matched tag.
        - protein amino acid composition
        - peptides from enzymatic digestion.

        url -> '\\data\\**H6KRX0*333*337'
        
        """
        FORM = """%s<b><font color="red">%s</font></b>%s"""
        alist = []
        url_params = url.split('*')
        # record = (Accession number, database entry title, protein sequence)
        record = self.dictionary[url_params[2]]
        #
        # ncbi has several consecutive titles separated with a
        # SOH, Start Of Heading
        titles = record[1].split(chr(1))

        for title in titles:
            # uniprot1 record[1]
            # >Q4U9M9|104K_THEAN 104 kDa micro/rot precursor - Opuntia adulate")
            title_items = title.split('|')
            #
            # when it is gi|23444553|sp|NP2345665
            if len(title_items) > 2:
                for i in range(int(len(title_items) / 2)):
                    alist.append(title_items[2 * i] + ' ' +
                                 title_items[2 * i + 1])
            else:
                alist.append(title_items[0])
            prot_info = title_items[-1]
            names = prot_info.split(';')
            for item in names:
                item = divide_text(item, 60)
                alist.append(item)
            alist.append(' ')
        #
        prot_seq = record[2]
        # noinspection PyArgumentEqualDefault
        mr_mono = get_mass(prot_seq, mode='mono', water=True)
        # noinspection PyArgumentEqualDefault
        mr_avg = get_mass(prot_seq, mode='avg', water=True)
        composition = Counter(prot_seq)
        #
        alist.append('\nMolecular weight (mono)= %.2f' % mr_mono)
        alist.append('Molecular weight (avg) = %.2f' % mr_avg)
        txt1 = '\n'.join(alist)
        #
        # Build protein sequence representation
        # (section of 10 aas with 5 sections per line)
        alist = []
        nmr = 0
        while nmr < len(prot_seq):
            seq = ''
            for i in range(5):
                seq += prot_seq[nmr:nmr + 10] + ' '
                nmr += 10
            alist.append(seq)
        txt2 = '\n'.join(alist)
        #
        # color the section in the above formatted protein sequence
        # corresponding to the query sequence
        ini = int(url_params[3]) - 1
        fin = int(url_params[4])
        # This is correct: an additional space every 10 (space in the printed
        # sequence, one more (2 in fact) every 50 (new line)
        start = ini + (ini // 10) + (ini // 50)  # int(ini*1.12) doesnt work
        stop = fin + (fin // 10) + (fin // 50)   # int(fin*1.12)

        txt2 = FORM % (txt2[:start], txt2[start:stop], txt2[stop:])
        self.enzyme = self.ps_cbboxes['enzyme'].GetValue()
        txt2 = mark_enzyme(txt2, self.enzyme)
        #
        lst_txt3 = ['%s:%s' % (item, composition[item]) for item in composition]
        txt3 = ', '.join(lst_txt3[:10]) + '\n' + ', '.join(lst_txt3[10:])
        #
        digest_sequences = digest(prot_seq, self.enzyme)
        txt4 = format_digestion_list(digest_sequences)
        #
        txt = '\n\n'.join((txt1, txt2, txt3, txt4))
        #
        self.show_results(txt, 'digest')
    #
    def show_results(self, txt, view_format='results'):
        """Show text <txt> in html page, saves it and record location."""
        
        if view_format == 'xml':
            txt = txt.replace('<', '<font color="red">&lt;')
            txt = txt.replace('>', '&gt;</font>')
        #
        if view_format == 'google':
            skltn = '<!DOCTYPE html><html><head><style>body{word-wrap:break-word; ' \
                    'font-family:courier;}</style></head><body>' \
                    '<font size=%i>%s</font></body></html>'
            self.html = skltn % (self.html_size, txt)
        else:
            self.html = '<PRE><font size=%i>%s</font></PRE>' % (self.html_size, txt)
        #
        fname = view_format + '.html'
        fpath = os.path.join(self.DIRQUIM, fname)
        #
        try:
            open(fpath, 'w').write(self.html)
        except UnicodeEncodeError:
            self.html = self.html.encode('latin1', 'ignore')
            open(fpath, 'wb').write(self.html)
     
        upath = 'file:///%s' % fpath
      
        if self.pn_html.location.FindString(upath) == -1:
            self.pn_html.location.Append(upath)

        self.current_path = upath
        self.pn_html.web_view.LoadURL(upath)
        self.pn_html.location.SetValue(upath)
    #
    def OnCloseWindow(self, event):
        """On close"""
        self.Destroy()
#
#
#
def search_mass_info(sequence):
    """Process lines with mass info in fasta type headers of sequence files.

    In a text containing a list of sequences that can be preceded by a line
    with an optional heading starting like

     - "> some_optional_string
       [123]ARTLESS[232]SNDR"

    - If the sequence has mass values in square bracket like in the lutefisk
    format, it splits the sequence in its tags, extracts the mass data with
    get_line() and returns a heading with the start and end masses of the tags:

     - "> optional_string Start# 123 End# 232
        ARTLESS
        > optional_string Start# 232 End#None
        SNDR"

    - If there is no mass data returns a heading like
        "> optional_string Start# None End# None"

    - If there is not heading it creates one:
        "> Start# None End# None"

    """
    #
    lines = sequence.splitlines()
    lines_new = []
    heading = ''
    mark = '>'
    has_heading = False
    heading_with_mass = False
    #
    for line in lines:
        if mark in line:
            has_heading = True
            heading = line
            heading_with_mass = ("Start#" in heading)
            continue
        else:
            splitted_line = get_line(line)
            more_than_one = (len(splitted_line) > 1)
            part = ' part' if more_than_one else ''
            # noinspection PyAssignmentToLoopOrWithParameter
            for line, mass in splitted_line:
                if has_heading and heading_with_mass:
                    head = heading
                else:
                    mass_info = ' Start# %s End# %s' % mass
                    if has_heading:
                        index = heading.find(' Start#')
                        if index < 0:
                            index = len(heading)
                        head = heading[:index] + part + mass_info
                    else:
                        head = mark + part + mass_info

                lines_new.append(head)
                lines_new.append(line)

        # comment if you want that, once a heading appears,
        # this heading to be considered common to all the following sequences
        # if they do not have their own heading.
        # Currently, the lines with sequence are considered independents,
        # so that there is no common heading, but a different one is created
        # for each sequence
        has_heading = False
    #
    return '\n'.join(lines_new)
#
#
def get_line(linea):
    """Extract mass tags from lutefisk type hits.

    It detects numbers between brackets in a text sequence.
    Uses regex COMPILED = re.compile("(\[(\d+)\])*([A-Z]*)")
    Returns the sequence and a list of tuples with the mass tags (strings)
    [123]ADDTAG[144]IMAM -->
    [('ADDTAG',('123','144')), ('IMAM', ('144', 'None'))]

    """
    text = []
    if '[' not in linea:
        return [(linea, (None, None))]
    matches = re.findall(COMPILED, linea)

    for index, match in enumerate(matches):
        start = match[1]
        if not start:
            start = None
        seq = match[2]

        if seq:
            end = matches[index + 1][1]
            if not end:
                end = None

            text.append((seq, (start, end)))

    return text
#
#
def divide_text(text, length, max_extend=25):
    """Split a text keeping parenthesized text in the same line.

    Lines with parenthesis longer than length+25 characters are split into
    lines that are cut at the next parenthesis after the position given by
    length.

    """
    if len(text) > (length + max_extend):
        idx = text.find('(', length)
        if idx >= 0:
            text = text[0:idx] + '\n' + divide_text(text[idx:], length)
    return text
#
#
# noinspection PyUnusedLocal
def run_blast(cbboxes, infile, outfile, dbase_path, log_txt):
    """Blast sequences in input file.

    Blast performs batch searches by default. It only needs that sequences in
    infile to be separated by a line starting with ">". These separation lines
    are generated automatically by search_mass_info().

    """
     
    program = cbboxes['blastype'].GetValue()
    view_format = cbboxes['format'].GetValue()
    kwargs = dict(kbc.config.items('blast type options'))

    local_args = dict(
        [('gapextend', cbboxes['gapextend'].GetValue()),
         ('matrix', cbboxes['matrix'].GetValue()),
         ('evalue', cbboxes['evalue'].GetValue()),
         ('word_size', cbboxes['word_size'].GetValue()),
         ('seg', cbboxes['seg'].GetValue()),
         ('outfmt', cbboxes['outfmt'].GetValue()),
         ('num_descriptions', cbboxes['num_descriptions'].GetValue()),
         ('num_alignments', cbboxes['num_alignments'].GetValue()),
         ('gapopen', cbboxes['gapopen'].GetValue()),
         ('ungapped', cbboxes['ungapped'].GetValue() != 'False')])

    kwargs.update(local_args)
    #
    if not os.path.exists(dbase_path):
        tell('      File %s doesn\'t exist\n'
             'you need to create it with makeblastdb.\n'
             '   Use KimBlast Setup Fasta tool'
             % dbase_path)
        return None
    #
    if view_format in ('custom', 'custom_bw', 'xml'):
        kwargs['outfmt'] = '5'
    else:
        outfmt = MyParams.blast_params['outfmt'].index(kwargs['outfmt'])
        if outfmt > 0:
            outfmt -= 1
        kwargs['outfmt'] = str(outfmt)
    #
    kwargs['html'] = 'F'
    if view_format == "html":
        kwargs['html'] = 'T'
    #
    if program in ('blastn',):
        kwargs.pop('matrix', None)
        kwargs.pop('seg', None)
    #
    blastpath = kbc.config.get('directories', 'BLASTPATH')
    #
    # default output is
    # - xml when custom or xml view
    # - html when txt or html view
    output, error = blast(query=infile, program=program,
                          blastpath=blastpath, database=dbase_path[:-4],
                          log_txt=log_txt, kwargs=kwargs)
    #
    # fpath
    #  - custom view -> kimblast_path/data/blast_out.custom (default)
    #  - xml view    -> kimblast_path/data/blast_out.xml
    #  - etc
    fout = os.path.splitext(outfile)[0]
    fpath = '%s.%s' % (fout, view_format)
    save_blast_output(output, fpath)
    return fpath, error
#
#
def mark_enzyme(sequence, enzyme_name='trypsin'):
    """Marks in bold the amino acids at the enzymatic cut.

    Forbidden cut sites are also labelled (TO-DO).

    """
    for item in Enzyme.cut[enzyme_name]:
        sequence = sequence.replace(item, '<b>' + item + '</b>')
    return sequence
#
#
def format_digestion_list(alist, length_min=3):
    """Pretty format a list of peptide sequences adding aa position and mass.

    format:
         1-12   ARDATH .........1234
        13-24   ADDFILE   ......4567
    Returns a string with the text.

    """
    list2 = []
    init = 1
    sep1 = ' ' * 3
    for item in alist:
        sep2 = sep1
        length = len(item)
        if length < length_min:
            init += length
            continue
        #
        # noinspection PyArgumentEqualDefault
        mass = '%8.2f' % (get_mass(item, water=True))
        #
        if length > 35:
            item = item[0:15] + '[...]' + item[-15:]
            sep2 = ''
        elif length > 31:
            sep2 = ' ' * (35 - length)
        #
        end = init + length - 1
        #
        dots = '.' * (32 - length)
        txt = '%5s%5s %s%s%s%s%s' % (init, end, item, sep1, dots, sep2, mass)
        list2.append(txt)
        #
        init = end + 1
    return '\n'.join(list2)
#
#
def get_html_from_search(query, headers_found, dbtype='uniprot'):
    """From alist of descriptors creates an html string.

    The string includes the link to go to the sequence data.
    This function is called by on_search
    Needs:
    - list of descriptors <headers_found>
      that contain the searched name <query>
    - self.dictionary
    It extract the key needed to search the data in the dictionary.

    """
    alist = []
    heading = '**Accession**\t**UniProt ID**\tProtein Name'
    alist.append('<font color="green">***%s***</font>\n' % query)
    alist.append('<font color="green">%s</font>\n' % heading)
    #
    # Uniprot1 title = >Q4U9M9|104K_THEAN 104 kDa micro pester - Tibia annular
    link_form = '<b><a href=**%s*1*0>%s</a></b>\t%s\t%s'
    if dbtype == 'uniprot1':
        for header in headers_found:
            refs, title2 = header.split(" ", 1)
            title_items = refs.split('|')
            title0 = title_items[0].lstrip('>')
            title1 = title_items[1]
            title0_long = title0.center(12, '*')
            #
            link = link_form % (str(title0), title0_long, title1, title2)
            #
            alist.append(link)

    # NCBI title = >gi|6273290|gb|AF191664.1|AF191664 Opuntia clavata...
    elif dbtype == 'ncbi':
        for header in headers_found:
            refs, title2 = header.split(" ", 1)
            title_items = refs.split('|')
            title1 = title_items[1]
            title0 = title_items[3]
            title0_long = title0.center(12, '*')
            #
            # ncbi has several definitions for each entry
            # separated with ASCII code 001 (SOH, start of heading)
            if chr(1) in title2:
                title = title2.split(chr(1))
                title2 = '%s  .... %i more proteins' % (title[0],
                                                        len(title) - 1)
            #
            link = link_form % (str(title0), title0_long, title1, title2)
            #
            alist.append(link)

    # Uniprot title = >tr|Q6SXN9|Q6SXN9_CITED Haptoglobin OS=Opuntia idelle
    elif dbtype == 'uniprot':
        for header in headers_found:
            refs, title2 = header.split(" ", 1)
            title_items = refs.split('|')
            title0 = title_items[1]
            title1 = title_items[2]
            title0_long = title0.center(12, '*')
            #
            link = link_form % (str(title0), title0_long, title1, title2)
            #
            alist.append(link)

    # IPI title =
    # >IPI:IPI00480882.5|TREMBL:Q1L8M6|ENSEMBL:ENSNARE00000094056|
    # REFSEQ:NP_001038533;XP_001337199|
    # VEGA:OTTERS00000012648 Tax_Id=7955 Gene_Symbol=LOC100000489;-->
    # -->si:ch211-240j22.2 Novel protein similar to vertebrate cyclin D2
    else:
        tell("%s still to be implemented" % dbtype)
        return ""

    return '\n'.join(alist)
#
#
def save_dbase_conf(dictionary):
    """Write config.txt file."""
    import shutil
    #
    db_conf_file = kbc.config.get('directories', 'DB_CONF_FILE')
    dbase_conf_bak = db_conf_file + '_bak'
    line_list = []
    for key in dictionary:
        csv_tuple = ','.join(list(dictionary[key]))
        line = key + ':' + csv_tuple
        line_list.append(line)
    txt = '\n'.join(line_list)
    #
    if os.path.exists(db_conf_file):
        shutil.copyfile(db_conf_file, dbase_conf_bak)
    open(db_conf_file, 'w').write(txt)
#
#
if __name__ == '__main__':
    app = wx.App()
    bf = BlastFrame(title=kbc.TITLE)
    bf.Show()
    # bf.Raise()
    # bf.SetFocus()
    # noinspection PyUnresolvedReferences
    app.MainLoop()
