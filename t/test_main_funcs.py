from unittest import TestCase
from kimblast.kblast_main import search_mass_info
from kimblast.kblast_main import get_line
from kimblast.kblast_main import divide_text
from kimblast.kblast_main import format_digestion_list

__author__ = 'joaquin'


# noinspection SpellCheckingInspection,PyRedundantParentheses
class TestMainFuncs(TestCase):
    def test_get_line(self):
        sequences = ['[123]ADDTAG[144]IMAM[666]',
                     'ADDTAG[144]IMAM',
                     'ADDTAGIMAM',
                     'ADDTAGIMAM[666]',
                     '[123]ADDTAGIMAM',
                     ]

        expecteds = [[('ADDTAG', ('123', '144')), ('IMAM', ('144', '666'))],
                     [('ADDTAG', (None, '144')), ('IMAM', ('144', None))],
                     [('ADDTAGIMAM', (None, None))],
                     [('ADDTAGIMAM', (None, '666'))],
                     [('ADDTAGIMAM', ('123', None))]
                     ]

        for sequence, expected in zip(sequences, expecteds):
            self.assertEqual(get_line(sequence), expected)

    def test_search_mass_info(self):
        sequences = [("> mi sequence\nADDTAG"),
                     ("> mi sequence\nADDTAG[144]IMAM"),
                     ]

        expecteds = [("> mi sequence Start# None End# None\nADDTAG"),
                     ("> mi sequence part Start# None End# 144\nADDTAG\n"
                      "> mi sequence part Start# 144 End# None\nIMAM"),
                     ]

        for sequence, expected in zip(sequences, expecteds):
            self.assertEqual(expected, search_mass_info(sequence))

    def test_divide_text(self):
        sequences = [
            ("> this is a long text which probably (should) be splitted"),
            ("> this is a long text (should) be splitted"),
            ("> this is a long text which is not splitted"),
            (""),
            ("1234567890123456789012345"),
            ("123456789012345678901234567890")
                     ]

        expecteds = [
            ("> this is a long text which probably \n(should) be splitted"),
            ("> this is a long text \n(should) be splitted"),
            ("> this is a long text which is not splitted"),
            (""),
            ("1234567890123456789012345"),
            ("123456789012345678901234567890")
                     ]
        for sequence, expected in zip(sequences, expecteds):
            self.assertEqual(expected, divide_text(sequence, 15))

    def test_format_digestion_list(self):
        sequences = [([u'MK', u'K', u'FIFIISIFLLLLSCGNGGS']),
                     ([])
                     ]

        #
        expecteds = [
            (u"    4   22 FIFIISIFLLLLSCGNGGS   .............    2013.10"),
            ("")]

        for sequence, expected in zip(sequences, expecteds):
            self.assertEqual(expected, format_digestion_list(sequence))
