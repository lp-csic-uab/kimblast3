---

**WARNING!**: This is the *Old* source-code repository for KimBlast3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/kimblast3/) located at https://sourceforge.net/p/lp-csic-uab/kimblast3/**  

---  
  
![https://lh3.googleusercontent.com/-OLOgNW6tIkU/TOQAarPMNFI/AAAAAAAAAFc/6JCwg5gVCh4/s800/KimBlast1.png](https://lh3.googleusercontent.com/-OLOgNW6tIkU/TOQAarPMNFI/AAAAAAAAAFc/6JCwg5gVCh4/s800/KimBlast1.png)
  
---

**WARNING!**: This is the *Old* source-code repository for KimBlast3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/kimblast3/) located at https://sourceforge.net/p/lp-csic-uab/kimblast3/**  

---  
  
  https://sourceforge.net/p/lp-csic-uab/kimblast3/https://sourceforge.net/p/lp-csic-uab/kimblast3/https://sourceforge.net/p/lp-csic-uab/kimblast3/https://sourceforge.net/p/lp-csic-uab/kimblast3/
**Table Of Contents:**

[TOC]

#### Description

**`KimBlast`** is a tool for performing [Blast (Basic Local Alignment Search Tool) searches](http://blast.ncbi.nlm.nih.gov/) locally with small peptide sequences, as it is common in proteomics _de novo_ MS/MS sequencing experiments. It has a full set of Blast options to optimize peptide searches and tools to facilitate fast analysis of the results.  

  This document refers to **`KimBlast 5`** the Python 3 updated version of the tool. No more development is expected in version < 4.

![https://lh3.googleusercontent.com/-35H8pX1UJHs/ToXfwF3FPMI/AAAAAAAAAOE/jVWET8U1DNM/s800/kimblast301_main.png](https://lh3.googleusercontent.com/-35H8pX1UJHs/ToXfwF3FPMI/AAAAAAAAAOE/jVWET8U1DNM/s800/kimblast301_main.png)

#### Features

  * Local Blast Analysis on individual or multiple sequences entered manually or in text files.
  * Batch search of  sets of sequence files.
  * Accepts query sequences in the format provided by the Lutefisk de novo search program (`[123]ASTR[234]TTDSCV[256]`).
  * Output results in conventional XML, HTML and text formats or `KimBlast` format
  * Export to Excel of `KimBlast` Blast results.
  * Search entries in fasta database for specific terms.
  * Filter Blast Results on the base of specific terms.
  * Complex text search and Blast results filtering using  AND, OR, NOT boolean operators.
  * Automatic multiple searchs considering all the combinations of Ile and Leu.
  * Direct google search in expasy and wikipedia of the text selected in navigator.
  * Direct connection to iHOP on the text selected in navigator.
  * Mass calculator.
  * Tool for formatting fasta files for Blast searchs.

![https://lh3.googleusercontent.com/-_tAMxOvWvlg/TpbhYLnkbtI/AAAAAAAAAOc/n0tDOZzpezI/s800/kimblast301_views.png](https://lh3.googleusercontent.com/-_tAMxOvWvlg/TpbhYLnkbtI/AAAAAAAAAOc/n0tDOZzpezI/s800/kimblast301_views.png)

#### Installation

Tested in Windows 10 64 bits.

##### Requirements

  * [NCBI BLAST](ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/): `KimBlast 4 and up` use the stand-alone NCBI **BLAST+** executables (last version tested is 2.2.28; for _legacy Blast_, stay with [version 3 of KimBlast](https://bitbucket.org/lp-csic-uab/kimblast/downloads/KimBlast_3.2.3_setup.exe)):
    1. Download the NCBI **BLAST+** application installer (ex. [ncbi-blast-2.2.28+-win32.exe](ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/ncbi-blast-2.2.28+-win32.exe)) from: [ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/](ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/)
    1. Create a folder **C:\Blast**
    1. Execute the installer in **C:\Blast** , or move the produced folders (bin, data and docs) to **C:\Blast**.
    1. Be sure **C:\Blast\bin** is in your PATH ( typing 'blastp' in the shell should give you a BLAST query/options error), otherwise edit your PATH environment variable ( `Control Panel -> System -> Advanced` ).

  * [Fasta Databases](http://en.wikipedia.org/wiki/FASTA_format) :
    1. Use your custom .fasta file or load it from a public repository such as [UniProt](http://www.uniprot.org/).
    1. Create a directory named **C:\Blast\DBs** and put your Fasta file there.
    
  * Google Custom Search Engine (Optional). **`KimBlast`** context search on Google uses the Google Custom Search Engine (CSE) API.  The CSE is a free service which allows 100 searchs per day.  
 To allow this tool you need:
  
    - A Google API key    
    API keys are obtained in the Credentials page of the Google API console.
    (https://developers.google.com/api-client-library/python/guide/aaa_apikeys)
    - A Custom Search Engine ID  
    The Search engine must be created in (https://cse.google.com/cse/all).
    
    See StackOverFlow Question 37083058 for more details  
    (https://stackoverflow.com/questions/37083058/programmatically-searching-google-in-python-using-custom-search).
    
    These two keys have to be stored on the configuration file commons.win.config (windows) or commons.posix.config (linux) in the API_KEY and USER_IP entries, respectively.
API_KEY=

##### From Installer

  1. Download `KimBlast_5.y.z_setup.exe` [Windows Installer](https://bitbucket.org/lp-csic-uab/kimblast/downloads).
  1. [Get your Password](#markdown-header-download) for the installer.
  1. Double click on the installer and follow the Setup Wizard.
  1. Run `KimBlast` by double-clicking on the `KimBlast` short-cut in your desktop or from the START-PROGRAMS application folder created by the installer.

##### From Source

  1. Install Python and third party software indicated in [Dependencies](#markdown-header-source-dependencies).
  1. Download `KimBlast` source code from its [Mercurial Source Repository](https://bitbucket.org/lp-csic-uab/kimblast3/src).
  1. Download commons source code from its [Mercurial Source Repository](https://bitbucket.org/lp-csic-uab/commons3/src).
  1. Copy the `KimBlast` and `commons` folders in your Python `site-packages` or in another folder in the `python path`. If you only plan to run the program from a particular user, you will need only to put `KimBlast` folder inside a user folder and then the `commons` folder inside `KimBlast` folder.
  1. Run `KimBlast` by double-clicking on the `kblast_main.pyw` module, or typing `python kblast_main.pyw` on the command line.

###### _Source Dependencies:_

  * [Python](http://www.python.org) 3.6 - 3.8 (not tested with other versions)
  * [wxPython](http://www.wxpython.org/) 4.0.6 (Phoenix)
  * [Biopython](http://www.biopython.org/) 1.76
  * [comtypes](https://pypi.python.org/pypi/comtypes/1.1.3) 1.1.3
  * [commons](https://bitbucket.org/lp-csic-uab/commons3) (from LP CSIC/UAB Google Code [repository](https://bitbucket.org/lp-csic-uab/commons3/src))

Third-party program versions correspond to those used for the installer (when) available here. Lower versions have not been tested, although they may also be fine.
> Note for developpers: Installation from source requires some small quirks in Biopython modules  'spark' and 'NCBIStandalone'.  Corrections must be done to prevent the breakdown of the executable (when prepared with py2exe) and the command shell opening when using Blast, respectively. (TO BE COMPLETED).

> `KimBlast` gives _'ImportError: cannot import name DistutilsOptionError'_ when installing `comtypes 0.6.2` in `python 2.7`. This is easily fixed with [this patch](http://sourceforge.net/p/comtypes/bugs/18/).

##### Post-Install

  * Before starting any work with the application you must install one or more of the Fasta files located in **C:\Blast\DBs** using the `KimBlast` **Fasta Database Setup** Tool as indicated [below](#markdown-header-how-to-format-and-index-new-fasta-databases).

  * On a fresh installation, the first time `KimBlast` is executed a directory named **C:\datos\_blast** is created. This directory can contain the following files:
    * _config.txt_ : File containing the name of the installed databases and the corresponding auxiliary format and index files. This file is generated with the Fasta Database Setup Tool [when setting up databases](#markdown-header-how-to-format-and-index-new-fasta-databases).
    * _batchfile_ : Generated after BLAST buttom is pressed.
    * _custom.html_ : Default search results file in HTML form. It is generated after BLAST button is pressed.
    * _blast\_temp_ : File with last query entered. It is the default input when BLAST button is pressed without specifying a query. It is generated after a query sequence is entered in the text box and saved.
    * _blast\_out.xml_ : Blast results in xml format. This file is generated after a query sequence is entered, saved and the BLAST button is pressed.

  * In **Windows 7 and up** you must grant writing privileges for the folder containing the application (**C:\Program Files (x86)\KimBlast\_X.Y.Z**). For this, go to the security tab in folder properties, edit permissions and check to allow writing. Writing privileges are required to create the log file in case of application failure.

#### How to Format and Index new Fasta Databases

  1. Get the new database-file in FASTA format and put it in **C:\Blast\DBs**. The application recognizes 3 "types" of Fasta format:
    * Old UniProt (uniprot1) :  `>Accession|EntryName ProteinName.....`.
    * UniProt (uniprot2):         `>sp|Accession|EntryName ProteinName.....`.
    * ncbi.
  1. Format and index the databases
    * Open the Fasta Setup dialog (The Fasta Setup Button becomes visible when the application is maximized).
    * Enter the name of the new database (Name).
    * Select the Fasta file (Fasta File).
    * Specify if the Fasta file is a protein or nucleotide file (Format, type).
    * Change the name of the .psq file if needed.
    * Select the type of fasta file (Index, type).
    * Change the name of the .idx file if needed.
    * Press Execute
    * Wait for complete generation of .nsq , .psq and .idx files in C:\blast\DBs
  1. Check that `KimBlast` works with the new database.
  1. The program will save the configuration of the new database when closed.

![https://lh4.googleusercontent.com/-xAKrIkYIEFo/TnjE9JJvdoI/AAAAAAAAAM8/K4-3M3IAJas/s288/FastaDBSetup.png](https://lh4.googleusercontent.com/-xAKrIkYIEFo/TnjE9JJvdoI/AAAAAAAAAM8/K4-3M3IAJas/s288/FastaDBSetup.png)

#### Download

You can download the Windows Installer for the last version of **`KimBlast`** [here](https://bitbucket.org/lp-csic-uab/kimblast/downloads)[![](https://lh6.googleusercontent.com/-LQE2us7J9GI/TnMstHYmquI/AAAAAAAAAKU/HgdPvan2S08/s800/downloadicon.jpg)](https://bitbucket.org/lp-csic-uab/kimblast/downloads).

After downloading the binary installer, you have to e-mail us at ![https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png](https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png) to get your free password and unlock the installation program. The password is not required to run the application from source code.

#### My installation does not work

If `KimBlast` does not start, crashes or malfunction (no windows system failure), it will produce a log file in the application folder indicating the problem. If you can not fix it by yourself send this file to us when requiring assistance.

The application requires several windows and python libraries (DLLs, See `install.txt` for a list of required libraries). Most of these libraries are already present in a conventional windows installation and we provide some of the libraries giving problems more often as well as some python DLLs. If you get a window error about _'library not found'_ you can try to download the required library from Internet.

#### Change-Log

5.0.1 April 2020
  * Several changes.
  * first exe version for py3 using pyinstaller.

5.0.0 December 2017
  * Migrated to Python3 and Phoenix
  * Harvester search replaced with iHOP search
  * Google search now works with a Google Custom Search Engine
  * A test framework has been initiated
  * Compatibility with Linux was initiated in version 4.0 (not tested on version 5)

4.0.2 August 2014

  * Add several more dlls required by py2exe
  * Add a new multi\_icon file to recover icon in win7

4.0.1 August 2014

  * Fix problem reading config files from the py2exe executable (fast and dirty executable for Oviedo)

4.0.0 September 2013 (new branch)

  * `KimBlast` now uses BLAST+
  * Configuration files added to:
  * Customize default selections on GUI.
  * Customize paths for BLAST, DBs, etc folders (Two versions of `KimBlast`, one for legacy BLAST and other for BLAST+ could coexist).
  * Set other BLAST parameters not exposed in GUI.

3.2.2 April 2012

  * Fix bug coming from nowhere that produced index error exception when some query in a list of queries gave no matches. That probably arose from [changes in | use of ](.md) biopython NCBIXML.parse that only produces records from queries with hits.
  * Docs updated to reflect changes in the name of fasta file types in the database setup panel.
  * Docs updated to be more precise on how to setup blast folder.

3.2.1 March 2012

  * Fixed unfriendly behavior (crash of Kimblast or Database Setup and making this dialog a Walking Dead...) when installation was made not following these doc instructions. Now the program catch errors in DatabaseSetup dialog when:
    * there is no Blast folder,
    * there is a config file with wrong number of terms (old format)
    * when format is executed and the selected fasta file does not exist
  * In Database Setup Panel (Index) now the _uniprot2_ format type is just _uniprot_ and the first Indexation format choice. The original _uniprot_ old format type now shows as _uniprot1_ that already was its code name
> If you want to continue using your old configuration file, it should be fixed just changing manually the _uniprot2_ name to _uniprot_ for the databases listed in that file
  * Many refactoring and English translation. Some few small bug fixes

3.2.0 October 2011

  * dialogs are now independent frames. Only one instance allowed.
  * The color code dialog shows now all the keys with more correct colors.
  * matches with an hydrolytic site at the C-terminal were not cut at that point but extended until the next one. Fixed.

3.1.0 September 2011

  * Many bugs fixed
  * Cleaning and beautifying source and GUI
  * Mass Calculator use is now better guided
  * Google Search substitutes yahoo. Currently a Google key is not needed yet.

3.00 April 2011

  * New dialog for formatting and indexation of databases that greatly simplify the original procedure.

2.09 November 2010

  * Fixed bug that caused the lost of good matches in "mutate+join" mode.

2.08 November 2010

  * Fixed bug introduced in 2.7 that did not allow formatting.
  * Migration to Mercurial (HG) control revision system.

2.07 August 2010

  * Using SVN to control changes in package.
  * Reading text for the calculator now clean numbers and other characters.

2.06 August 2010

  * Many changes, be careful with bugs.
  * Changed the name of the application. It is now named `KimBlast` witch is more consistent with the logo and clarifies that it is not Blast.
  * The application now comes in an installer that provides the necessary DLLs.
  * The calculator has been improved and some bugs removed. Now gives the molecular weight directly. If you want to calculate a terminal modification, the water is first removed and then modification is added. The molecular weight is given with one more decimal place, in response to performance improvements with Orbitrap mass spectrograph.
  * Updated the code to the new versions of wxPython and Python26. In Biopython 1.54 corrections to 'spark' and 'NCBIStandalone' must be done to prevent the breakdown of the executable and the command shell opening when using Blast, respectively.
  * It remains as an unsolved problem the search with Harvester. The result web page fails. Sometimes it works and sometimes it hangs. The problem is one of the websites where the Harvester is searching; the developer has done a magic trick to do something that on the MS Internet Explorer can not be done. The guy is very clever, but it turns out that the tick produces JavaScript errors. Using directly the Internet Explorer it seems that the error is filtered, but not when using it from wxPython. One solution would be to open the page in the browser, but this is not beautiful.

2.05 May 2009

  * Several problems have been fixed:
    * Reading the input and output files.
    * Notice about the need to save if the query is changed.
    * Presentation of URLs in the MS Internet Explorer panel.
    * Correction of the presentation for the list of digestions.
    * Now the parameter gapped(T/F) is taken into account.
    * Bug introduced in 4.2 that loosed one of the selector DBs.
  * Compiled with Biopython 1.50 after correcting code NCBIStandalone (cmd opening when calling Blast) and parser (runtime error).

2.04

  * Fixed a bug in the fix tool that was deleting the first letter of the sequence in the editor.
  * Wordsize default is now set to 2.
  * Database classes have been reorganized.
  * It seems that now packaging with py2exe is very slow.
  * Note: Warning, Biopyton v. 1.49 gives problems (command shell when invoking Blast). Package or execute it with old Biopython. The current package uses release 1.45 of Biopython.

2.03

  * It is now able to read Lutefisk type tags such as `[123]ABCD[444]SDF[111]`. In this case it divides the original tag into two tags and stores the mass information at the heading line of the sequence.
  * A "custom\_bw" mode has been included. It only gives the match Blast, and without coloring.
  * Fixed some bugs.
  * iTRAQ has been included into the calculator.
  * blastpgp and rpsblast have been included, although I don't know very well how they work.

2.02

  * This new version uses a new ActiveX control for web presentation.
  * It includes a tool to mutate Leu (Peaks only generates Leu)

#### To-Do

  * Interface refinements.
  * Opening sequence files with the executable doesn't start in the same directory than when doing it from source (?).

#### Contribute

These programs are made to be useful. If you use them and don't work entirely as you expect, let us know about features you think are needed, and we will try to include them in future releases.

![https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png](https://lh3.googleusercontent.com/-0dLyX150-Aw/TncRPIeDRCI/AAAAAAAAAKo/6_9--dCM1WU/s800/contact_samll.png)
  
  
---

**WARNING!**: This is the *Old* source-code repository for KimBlast3 program from [LP-CSIC/UAB](http:proteomica.uab.cat).  
**Please, look for the [_New_ SourceForge _repository_](https://sourceforge.net/p/lp-csic-uab/kimblast3/) located at https://sourceforge.net/p/lp-csic-uab/kimblast3/**  

---  
  
