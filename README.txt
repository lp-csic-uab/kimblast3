KimBlast vs 5.0.1 (version coronavirus april 14, 2020).

Note this README file is seldom updated.
Updated info should be found in the application 'About'
and in the KimBlast site at google code.

 KimBlast has been coded in Python
  by Joaquin Abian at LP-CSIC/UAB
     ( lp.csic@uab.cat )

         April 2020



KimBlast
A tool for performing Blast searches with small peptide sequences
as it is common in proteomics de novo MS/MS sequencing experiments.


Table of contents
-----------------
1.- DEPENDENCIES
2.- INSTALLATION
3.- HOW TO FORMAT AND INDEX NEW DATABASES
4.- FEATURES AND CHANGELOG
5.- TO-DO
6.- CONTRIBUTE


DEPENDENCIES
----------------
If you work with Kimblast source you need Python installed (tested with Python
version 3.6 to 3.8) as well as the following dependencies:
    Biopython 1.76 -
    wxPython 4.0.6 (Phoenix) -
    commons3 (from lpcsicuab bitbucket repository)


INSTALLATION
----------------
 Tested in win7 64 bits and win10 64 bits

1) Install NCBI Blast
    Kimblast vs 5.0 and up use the stand-alone NCBI BLAST+ executables.
    Last version tested is 2.2.28.
    a.- Download the application installer from:
        ftp://ftp.ncbi.nih.gov/blast/executables/blast+/
    b.- Install the application in C:\Blast

2) Load fasta databases
    a.- Use your custom .fasta database or load it from a public
        repository such as Uniprot...
    b.- Create a directory C:\Blast\DBs and unzip your database there.

3a) Install Kimblast FROM WINDOWS INSTALLER
    a.- Download KimBlast_x.y_setup.exe installer from repository.
    b.- Double click on the installer in any directory.
    c.- Run Kimblast by double-clicking on the KimBlast direct access in
        your desktop or from the START-PROGRAMS application created by the
        installer.

3b) Install Kimblast FROM SOURCE (when available)
    a.- Download Kimblast and commons zip archives from repository
    b.- Unzip in python site-packages or in another folder in the python path
    c.- Run Kimblast by double-clicking on the kblast_main.pyw module.

3c) Install Kimblast FROM MERCURIAL
    a.- Get a copy of the KimBlast and commons repositories from mercurial:
        http://code.google.com/p/lp-csic-uab/source/checkout?repo=kimblast
        http://code.google.com/p/lp-csic-uab/source/checkout?repo=commons
    b.- Locate de folders and run the application as indicated in 3b.

4) Post-Install
    Before starting any work with the application you must install one or more
    of the fasta databases located in C:/Blast/DBs using the KimBlast Setup
    dialog as indicated below.

    On a fresh instalation the first time KimBlast is executed a directory
    'path_2_app/data' is created. This directory can contain the following files:
    - config.txt   containing the name of the installed databases and
                       the corresponding auxiliary format and index files.
                       Generated with the Setup Fasta Tool.
    - batchfile     Generated after BLAST buttom is pressed.
    - custom.html   Default search results file in HTML form.
                          Generated after BLAST buttom is pressed.
    - blast_temp    File with last query entered. It is the default input when
                          BLAST button is pressed without specifying a query.
                          Generated after a query sequence is entered in the
                          text box and saved.
    - blast_out.xml  Blast results in xml format.
                          Generated after a query sequence is entered, saved
                          and the 'BLAST' buttom is pressed.

    Installation from source requires some small quirks in Biopython modules
    'spark' and 'NCBIStandalone'.  Corrections must be done to prevent the
    breakdown of the executable (if prepared with py2exe) and the command shell
    opening  when using Blast, respectively. (TO BE COMPLETED).


HOW TO FORMAT AND INDEX NEW DATABASES
-------------------------------------

1) Get the new database in FASTA format.
   The application recognizes and indexes 3 types of fasta formats:
    a. - Old Uniprot (uniprot1) :    ">Accession|EntryName ProteinName.....".
    b. - Current UniProt (uniprot): ">sp|Accession|EntryName ProteinName.....".
    c. - ncbi.

2) Format and index the database:
    a.- Open the Fasta Setup dialog (The Fasta Setup Button becomes visible
        when the application is maximized.
    b.- Enter the name of the new database (Name).
    c.- Select the fasta file (Fasta File).
    d.- Specify if the database is a protein or nucleotide file (Format, type).
    e.- Change the name of the .psq file if needed.
    f.- Select the type of fasta file (Index, type).
    g.- Change the name of the .idx file if needed.
    h.- Press Execute

    Wait for complete generation of files
    *.nsq/.psq and *.idx  in C:\blast\DBs

3) Check that KimBlast works with the new database.

4) The program will save the configuration of the new database when closed.


FEATURES AND CHANGELOG
-----------------------
5.0.1 April 2020
- Several changes
- first exe version for py3

5.0.0 December 2017
- Migrated to Python3 and Phoenix
- Harvester search replaced with iHOP search
- Google search now works with a Google Custom Search Engine
- A test framework has been initiated
- Compatibility with Linux was initiated in version 4.0 (not tested on version 5)

4.0.2 August 2014
- Add several more dlls required by py2exe

4.0.1 August 2014
- Fix problem reading config files from the py2exe executable
(fast and dirty executable for oviedo)

4.0.0 September 2013 (new branch)
- KimBlast now uses BLAST+
  Configuration files added to:
     -Customize default selections on GUI.
     -Customize paths for BLAST, DBs, etc folders
      (Two versions of KimBlast, one for legacy BLAST and other for BLAST+ could coexist).
     -Set other BLAST parameters not exposed in GUI.

3.2.2 April 2012
- Fix bug coming from nowhere that produced index error exception when some query in a list of queries gave no matches.
  That probably arose from [ changes in | use of ] biopython NCBIXML.parse that only produces records from queries with hits.
- Docs updated to reflect changes in the name of fasta file types in the database setup panel.
- Docs updated to be more precise on how to setup blast folder.

3.2.1 March 2012
- Fixed  unfriendly behavior (crash of Kimblast or Database Setup and making
  this dialog a Walking Dead...) when installation was made not following these
  doc instructions. Now the  program catch errors in DatabaseSetup dialog when: 
    - there is no Blast folder,
    - there is a config file with wrong number of terms (old format)
    - when format is executed and the selected fasta file does not exist 
-  In Database Setup Panel (Index) now the uniprot2 format type is just uniprot
   and the first Indexation format choice. The original uniprot old format type
   now shows as uniprot1 that already was its code name
   If you want to continue using your old configuration file, it should be fixed
   just changing manually the uniprot2 name to uniprot for the databases listed
   in that file
-  Many refactoring and English translation. Some few small bug fixes

3.2.0 october 2011
- dialogs are now independent frames. Only one instance allowed.
- The color code dialog shows now all the keys with more correct colors.
- matches with an hydrolitic site at the C terminal were not cut at that point
  but extended until the next one. Fixed.

3.01 september 2011
- Many bugs fixed
- Cleaning and beautifying source and GUI
- Mass Calculator use is now better guided
- Google Search substitutes yahoo. Currently a google key is not needed yet.

3.00 april 2011
- New dialog for formatting and indexation of databases that greatly
  simplify the original procedure.

2.09 November 2010
- Fixed bug that caused the lost of good matches in �mutate+join� mode.

2.08 November 2010
- Fixed bug introduced in 2.7 that did not allow formatting.
- Migration to Mercurial (HG) control revision system.

2.07 August 2010
- Using SVN to control changes in package.
- Reading text for the calculator now clean numbers and other characters.

2.06 August 2010
- Many changes, be careful with bugs.
- Changed the name of the application. It is now named Kimblast wich is
  more consistent with the logo and clarifies that it is not Blast.
- The application now comes in an installer that provides the necessary dlls.
- The calculator has been improved and some bugs removed.
  Now gives the molecular weight directly. If you want to calculate a
  terminal modification, the water is first removed and then modification is
  added.
  The molecular weight is given with one more decimal place, in response to
  performance improvements with  Orbitrap mass spectrograph.
- Updated the code to the new versions of wxPython and Python26.
  In Biopython 1.54 corrections to 'spark' and 'NCBIStandalone' must be done
  to prevent the breakdown of the executable and the command shell opening
  when using Blast, respectively.
- It remains as an unsolved problem the search with harvester. The result
  web page fails. Sometimes it works and sometimes it hangs.
  The problem is one of the websites where the harvester is searching; the
  developer has done a magic trick to do something that on the MS Internet
  Explorer can not be done. The guy is very clever, but it turns out that the
  tick produces Javascript errors.
  Using directly the Internet Explorer it seems that the error is filtered,
  but not when using it from wxPython. One solution would be to open the page
  in the browser, but this is not beautiful.

2.05 May 2009
- Several problems have been fixed:
	- Reading the input and output files.
	- Notice about the need to save if the query is changed.
	- Presentation of URLs in the MS Internet Explorer panel.
	- Correction of the presentation for the list of digestions.
	- Now the parameter gapped(T/F) is taken into account.
	- Bug introduced in 4.2 that loosed one of the selector DBs.
- Compiled with Biopython 1.50 after correcting code NCBIStandalone
  (cmd opening when calling Blast) and parser (runtime error).

2.04
- Fixed a bug in the fix tool that was deleting the first letter of the sequence
  in the editor.
- Wordsize default is now set to 2. 
- Database classes have been reorganized.
- It seems that now packaging with py2exe is very slow.
- Note: Warning, Biopyton v. 1.49 gives problems (command shell when
  invoking Blast). Package or execute it with old Biopython. The current
  package uses release 1.45 of Biopython.

2.03
- It is now able te read Lutefisk type tags such as [123]ABCD[444]SDF[111].
  In this case it divides the original tag into two tags and stores the
  mass information at the heading line of the sequence.
- A "custom_bw" mode has been included. It only gives the match Blast, and
  without coloring.
- Fixed some bugs.
- iTRAQ has been included into the calculator.
- blastpgp and rpsblast have been included, although I don't know very well
  how they work.

2.02
- This new version uses a new ActiveX control for web presentation.
- It includes a tool to mutate Leu (Peaks only generates Leu)


TO-DO
--------

- Some ideas ?


CONTRIBUTE
-----------

These programs are made to be useful. If you use them and don't work
entirely as you expect, let me know about features you think are needed, and
I'll try to include them in future releases.

Joaquín Abián April 2020