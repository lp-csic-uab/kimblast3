#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
#
"""
kblast setup frame
"""
#
import wx
import os
import copy
import subprocess
from sqlite3 import OperationalError
from commons.warns import tell
from kblast_setupbaseframe import SetupBaseFrame
from kblast_cls_dbase import DataBase

# noinspection PyArgumentList
# noinspection PyUnusedLocal
class SetupFrame(SetupBaseFrame):
    """Format and index dialog.
    Returns new dictionary to parent window
    """
    exists = False

    NCBI = ("For NCBI dbase NCBI of the type: \n",
            ">gi|6273290|gb|AF191664.1|AF191664 Opuntia clavata...")
    UNIP1 = ("For old UNIPROT dbase of the type: \n",
             ">Q4U9M9|104K_THEAN 104 kDa macrame/rhody prc - Ter anita")
    UNIP = ("For current UNIPROT dbase of the type: \n",
            ">sp|Q4U9M9|104K_THEAN 104 kDa macrame/rhody prc - Ter anita")
    #
    GNAME = ('Given name for the database that will appear in the selector\n\n'
             'Press Enter to accept')
    FNAME  = 'Name of the fasta file to format and index'
    FTNAME = 'Name of the makeblastdb formatted file'
    INAME  = 'Name for the index file'
    CHANGE = 'Change default name'
    EXECUTE = 'Format and Index'
    CANCEL = 'Cancel'

    index_hints = [(0, ''.join(UNIP)),
                   (1, ''.join(UNIP1)),
                   (2, ''.join(NCBI))
                   ]
    format_hints = [(0, 'For fasta files with protein sequences'),
                    (1, 'For fasta files with nucleotide sequences')
                    ]

    hints = [('cbbx_name', GNAME), ('cbbx_fasta', FNAME),
             ('tc_format_file', FTNAME), ('tc_idx_file', INAME),
             ('bt_format_file', CHANGE), ('bt_index_file', CHANGE),
             ('bt_execute', EXECUTE), ('bt_cancel', CANCEL),
             ]

    def __init__(self, parent, info_dtbs=None, db_path=None, blast_path=None, log=None):
        """init"""
        SetupBaseFrame.__init__(self, None)
        self.parent = parent

        if db_path and os.path.exists(db_path):
            self.db_path = db_path
        else:
            tell("databases Folder <db_path> could not be found.\n"
                 "Please follow installation instructions")
            self.bt_execute.Disable()

        self.blast_path = blast_path
        self.dtbs = copy.deepcopy(info_dtbs)
        self.log = log
        self.setup_db_name = ''
        self.name_selected = False
        #
        self.Bind(wx.EVT_CLOSE, self.on_close)
        #
        self.fastas = []
        self.fasta = ''
        self.format_file = ''
        self.format_type = self.rbx_format.GetStringSelection()
        self.idx_file = ''
        self.index_type = self.rbx_index.GetStringSelection()
        #
        self.fill_comboboxes()
        self.set_tooltips()
        self.tc_format_file.Disable()
        self.tc_idx_file.Disable()
        SetupFrame.exists = True
    #
    def set_tooltips(self):
        """Set widgets tooltips"""
        for item, tooltip in self.index_hints:
            self.rbx_index.SetItemToolTip(item, tooltip)

        for item, tooltip in self.format_hints:
            self.rbx_format.SetItemToolTip(item, tooltip)

        for attrib, tooltip in self.hints:
            widget = getattr(self, attrib)
            # noinspection PyUnresolvedReferences
            widget.SetToolTip(tooltip)
    #
    def refresh(self):
        """"""
        self.get_setup_db_data()
        self.fill_setup_widgets()
    #
    def get_setup_db_data(self):
        """"""
        try:
            values = self.dtbs[self.setup_db_name]

            self.fasta = values[0]
            self.format_file = values[1]
            self.idx_file = values[2]
            self.format_type = values[3]
            self.index_type = values[4]
        except (IndexError, AttributeError):
            tell('Something when wrong whith your config file entry for the'
                 'current database <%s>' % self.setup_db_name)

    def fill_comboboxes(self):
        """"""
        self.fill_cbbx_fastas()
        self.fill_cbbx_names()

    def fill_cbbx_fastas(self):
        """Fill combobox selector with fasta file names in database directory"""
        archives = os.listdir(self.db_path)
        self.fastas = [arch for arch in archives if arch.endswith('.fasta')]
        self.cbbx_fasta.SetItems(self.fastas)

    def fill_cbbx_names(self):
        """Fill name combobox selector with given names of databases"""
        names = sorted(self.dtbs.keys())
        self.cbbx_name.SetItems(names)

    def fill_setup_widgets(self):
        """Setup widgets contents/state with current data"""
        self.cbbx_name.SetStringSelection(self.setup_db_name)
        self.lb_fasta_dir.SetLabel(self.db_path)
        self.cbbx_fasta.SetStringSelection(self.fasta)
        self.tc_format_file.SetValue(self.format_file)
        self.tc_idx_file.SetValue(self.idx_file)
        self.rbx_format.SetStringSelection(self.format_type)
        self.rbx_index.SetStringSelection(self.index_type)

    def on_select_name(self, evt):
        """on given name selected"""
        self.name_selected = True
        self.setup_db_name = self.cbbx_name.GetStringSelection()
        self.refresh()

    def on_evt_text_in_name(self, evt):
        """on text entered in database given names selector"""
        # this prevents recursive calls after selecting a given name
        if self.name_selected:
            self.name_selected = False
        else:
            self.cbbx_name.SetBackgroundColour('red')
            self.cbbx_name.Refresh()

    def on_set_name(self, evt):
        """set given name in selector and updates data after pressing enter"""
        # validate name
        t = str.maketrans('', '', ' :.|,;')
        name = self.cbbx_name.GetValue()
        name = name.translate(t)
    
        self.cbbx_name.SetValue(name)
        self.cbbx_name.SetInsertionPointEnd()

        self.name_selected = False
        self.cbbx_name.SetBackgroundColour('white')
        self.cbbx_name.Refresh()

        self.setup_db_name = self.cbbx_name.GetValue()
        if self.setup_db_name in self.dtbs:
            self.refresh()

    def on_select_fasta(self, evt):
        """on a fasta file selected"""
        self.fasta = self.cbbx_fasta.GetStringSelection()

        (name, _) = os.path.splitext(self.fasta)
        self.idx_file = '%s.idx' % name

        if self.format_type == 'protein':
            self.format_file = '%s.psq' % name
        if self.format_type == 'nucleotide':
            self.format_file = '%s.nsq' % name

        self.fill_setup_widgets()

    def on_change_format_file(self, evt):
        """"""
        tell('no implemented yet')

    def on_change_index_file(self, evt):
        """"""
        tell('no implemented yet')

    def on_format_type(self, evt):
        """on selecting an option in the format type radiobox"""
        self.format_type = self.rbx_format.GetStringSelection()
        self.fasta = self.cbbx_fasta.GetStringSelection()

        (name, _) = os.path.splitext(self.fasta)

        if self.format_type == 'protein':
            self.format_file = '%s.psq' % name
        if self.format_type == 'nucleotide':
            self.format_file = '%s.nsq' % name

        self.tc_format_file.SetValue(self.format_file)

    def on_index_type(self, evt):
        self.index_type = self.rbx_index.GetStringSelection()

    def on_execute_setup(self, evt):
        """on pressing execute button"""
        FORMAT_EXE = os.path.join(self.blast_path, 'makeblastdb')
        fasta = os.path.join(self.db_path, self.fasta)
        if not os.path.exists(fasta):
            tell("No fasta file %s exists." % fasta)
            return
        format_filename = self.format_file.rsplit('.', 1)[0]
        format_filepath = os.path.join(self.db_path, format_filename)
        idx_filepath = os.path.join(self.db_path, self.idx_file)
        format_type = 'prot' if self.format_type == 'protein' else 'nucl'

        for path in (FORMAT_EXE, fasta, format_filepath):
            for char in ",:;":
                if char in path[2:]:
                    msg = ("Inadequate file path:\n%s\n\n"
                           "Please, do not use names with spaces "
                           "or other bizarre characters") % path
                    tell(msg, 'File Name Error')
                    return

        format_cmd = '%s -in %s -dbtype %s -out %s' % (FORMAT_EXE, fasta,
                                                       format_type,
                                                       format_filepath)

        bt_label = self.bt_execute.GetLabel()
        bt_color = self.bt_execute.GetBackgroundColour()
        self.manage_execute_button('red', 'Working...', wx.CURSOR_WAIT)

        try:
            dbase = DataBase(idx_filepath)
            dbase.fill(fasta, self.index_type)
            dbase.close()
        except OperationalError as err:
            tell(str(err))   # no database. Unable to open database file
            self.manage_execute_button(bt_color, bt_label, wx.CURSOR_DEFAULT)
            return
        #
        self.log(format_cmd)
        #
        # https://github.com/pyinstaller/pyinstaller/wiki/Recipe-subprocess
        if 'nt' in os.name:
            si = subprocess.STARTUPINFO()
            si.dwFlags |= subprocess.STARTF_USESHOWWINDOW
        else:
            si = None

        # https://github.com/pyinstaller/pyinstaller/issues/1339
        try:
            process = subprocess.Popen(format_cmd,
                                       stdin=subprocess.PIPE,
                                       stdout=subprocess.PIPE,
                                       stderr=subprocess.PIPE,
                                       shell='posix' in os.name,
                                       startupinfo=si)
        except (OSError, TypeError) as e:
            msg = "Error: {0}".format(e)
            tell(msg, 'makeblastdb error')
            self.log(msg)
            self.manage_execute_button(bt_color, bt_label, wx.CURSOR_DEFAULT)
            return
        else:
            # noinspection PyUnusedLocal
            out, err = process.communicate()
        #
        if err:
            msg = ("Error executing '%s':\n"
                   "%s\n\n"
                   "Please, make sure you have it correctly installed."
                   ) % (FORMAT_EXE, err)
            tell(msg, 'Execution Error!', 'fatal')
            self.log('ERROR: makeblastdb execution error')
        else:
            dtb = {self.setup_db_name: (self.fasta, self.format_file,
                                        self.idx_file, self.format_type,
                                        self.index_type)}
            self.dtbs.update(dtb)
            self.update_parent_dtbs()
            tell("Finished formating file\n'%s'" % fasta)
        #
        self.manage_execute_button(bt_color, bt_label, wx.CURSOR_DEFAULT)

    def manage_execute_button(self, color, label, cursor):
        self.bt_execute.SetLabel(label)
        self.bt_execute.SetBackgroundColour(color)
        self.bt_execute.Refresh()
        self.bt_execute.Update()
        self.SetCursor(wx.Cursor(cursor))
        self.Enable()
        self.Update()

    def update_parent_dtbs(self):
        self.parent.dict_dtbs = copy.deepcopy(self.dtbs)
        self.parent.update_dictionary_choices()

    def on_cancel(self, evt):
        SetupFrame.exists = False
        self.Destroy()

    # noinspection PyUnusedLocal
    def on_close(self, evt):
        self.on_cancel(None)
#
#
#
if __name__ == '__main__':
    
    import kblast_common as kbc
    BLASTPATH = kbc.config.get('directories', 'BLASTPATH')
    DBPATH = kbc.config.get('directories', 'DBPATH')

    # noinspection PyArgumentList,PyUnusedLocal
    class MyFrame(wx.Frame):

        DICT_DTBS = {'solea': ('solea.fasta',
                               'solea.psq', 'solea.idx', 'protein', 'uniprot1'),
                     'two': ('two.fasta',
                             'two.psq', 'two.idx', 'nucleotide', 'uniprot')}

        def __init__(self, *args, **kwds):
            kwds["style"] = wx.DEFAULT_FRAME_STYLE
            wx.Frame.__init__(self, *args, **kwds)
            self.button_1 = wx.Button(self, -1, "child")
            self.button_2 = wx.Button(self, -1, "show")

            self.__do_layout()

            self.Bind(wx.EVT_BUTTON, self.on_child, self.button_1)
            self.Bind(wx.EVT_BUTTON, self.on_show, self.button_2)

            self.SetTitle("Test Frame")

        def __do_layout(self):
            sizer_1 = wx.BoxSizer(wx.VERTICAL)
            sizer_2 = wx.BoxSizer(wx.HORIZONTAL)
            sizer_2.Add(self.button_1, 0, 0, 0)
            sizer_2.Add(self.button_2, 0, 0, 0)
            sizer_1.Add(sizer_2, 0, wx.EXPAND, 0)
            self.SetSizer(sizer_1)
            sizer_1.Fit(self)
            self.Layout()

        def on_child(self, event):
            child = SetupFrame(self,
                               info_dtbs=self.DICT_DTBS,
                               db_path=DBPATH,
                               blast_path=BLASTPATH)
            child.Show()

        def on_show(self, event):
            print(self.DICT_DTBS)
    #
    #
    app = wx.App(0)
    wx.InitAllImageHandlers()
    frame_1 = MyFrame(None, -1, "")
    app.SetTopWindow(frame_1)
    frame_1.Show()
    app.MainLoop()
