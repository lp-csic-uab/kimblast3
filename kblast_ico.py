#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
"""
kblast_ico
"""
#
ICON = """AAABAAEAICAQAAAAAADoAgAAFgAAACgAAAAgAAAAQAAAAAEABAAAAAAAgAIAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAACAAACAAAAAgIAAgAAAAIAAgACAgAAAgICAAMDAwAAAAP8AAP8AAAD//wD/AAAA
/wD/AP//AAD///8Au7u7u7u7u7u7u7u7u7u7u7AAAAAAAAiLu7u7u7u7u7u1WZmTu7szZsu7u7u7
u7u7tVmZk7u7M2bMu7u7u7u7u7VZmZO7uzNmzMu7u7u7u7u1Wbu7u7u7tszFu7u7u7u7tVm7u7u7
u7vMxQu7u7u7u7VZu1kLsmwLvMVbWbmwa8W1WbtZCzJsu7vF21m5sGvFtVm7WZsya7u7xVtZubBr
xbVZu1mbO7u7u8VbWbmza8W1WbtZmzK7u7vFW1m5k2zFtVm7WQAya7uwxVu7u7u7u7VZu1kLMmy7
vMVbWbu7u7u1WbtZC7Jsu8zFW1m7u7u7tVm7u7u7u2bMxbu7u7u7u7VZmZO7uzNmzMu7u7u7u7u1
WZmTu7szZsy7u7u7u7u7tVmZk7u7M2a7u7u7u7u7u7VZC7u7CzNmy7u7u7u7u7u1WQu7u7u7Zsy7
u7u7u7u7tVkLu7u7u7bMu7u7u7u7u7VZC7u7u7u2zMu7u7u7u7u1WQu7u7u7tszLu7u7u7u7tVkL
u7u7u7bMy7u7u7u7u7VZC7u7u7u2zMu7u7u7u7u1WQu7u7u7BszLu7u7u7u7tVkLu7u7A2bMu7u7
u7u7u7VZmZO7uzNmzLu7u7u7u7u1WZmTu7szZsu7u7u7u7u7tVmZk7u7M2u7u7u7u7u7u7u7u7u7
u7u7u7u7u7u7u7sAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=="""
#
#
if __name__ == "__main__":
    import wx
    from commons.iconic import MyIconTest

    class MyApp(wx.App):
        def OnInit(self):
            wx.InitAllImageHandlers()
            my_frame = MyIconTest(None, icon=ICON)
            my_frame.Show()
            return 1
    #
    #
    app = MyApp(0)
    app.MainLoop()
