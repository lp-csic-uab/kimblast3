#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
#
"""
kblast_funcs
"""
#
import os
from commons.warns import tell
from re import findall as re_find_all
from re import I as IGNORE_CASE
from re import compile as recompile
from Bio.Application import ApplicationError
import Bio.Blast.Applications as BioApps
from Bio.Blast import NCBIXML
#
# noinspection PyClassHasNoInit
class Enzyme:
    """Hydrolysis amino acid targets and inhibitors for common enzymes"""
    cut = {'trypsin': ('K', 'R'),
           'endolys': ('K',),
           'glu_c': ('D', 'E'),
           'none': ()
           }
    #
    forbidden = {'trypsin': ('KP', 'RP'),
                 'endolys': (),
                 'glu_c': (),
                 'none': ()
                 }
#
#
def blast(query="data\\secfile", program="blastp", blastpath='',
          database="data\\demo_databases\\test", log_txt=None, kwargs=None):
    """Run Blast programs and returns a string with the results

    - NcbiblastpCommandline - Protein-Protein BLAST
    - NcbiblastnCommandline - Nucleotide-Nucleotide BLAST
    - NcbiblastxCommandline - Translated Query-Protein Subject BLAST
    - NcbitblastnCommandline - Protein Query-Translated Subject BLAST
    - NcbitblastxCommandline - Translated Query-Protein Subject BLAST
    - NcbipsiblastCommandline - Position-Specific Initiated BLAST
    - NcbirpsblastCommandline - Reverse Position Specific BLAST
    - NcbirpstblastnCommandline - Translated Reverse Position Specific BLAST
    - NcbiblastformatterCommandline -Convert ASN.1 to other BLAST output formats

    """
    blast_programs = {'blastp': BioApps.NcbiblastpCommandline,
                      'blastp-s': BioApps.NcbiblastpCommandline,
                      'blastn': BioApps.NcbiblastnCommandline,
                      'blastx': BioApps.NcbiblastxCommandline,
                      'tblastn': BioApps.NcbitblastnCommandline,
                      'tblastx': BioApps.NcbitblastxCommandline,
                      }
    if kwargs is None:
        kwargs = {}

    blast_func = blast_programs[program]
    command = os.path.join(blastpath, program.split('-')[0])
    try:
        cline = blast_func(cmd=command, query=query, db=database, **kwargs)
    except ValueError as e:
        tell(str(e.args))
        return '', None

    #
    log_txt(str(cline))
    #    
    if cline:
        try:
            out, error = cline()
        except ApplicationError as e:
            process_app_error(e)
            return '', None
    else:
        return '', None
    #
    return out, error
#
#
def process_app_error(e):
    """Give adequate message error from blast applications.
    """

    errors = {1: 'Error in query sequence(s) or BLAST options',
              2: 'Error in BLAST database',
              3: 'Error in BLAST engine',
              4: 'Out of memory',
              255: 'Unknown error'}

    # text = e.stderr
    # if 'Warning:' in text:
    #     start = text.find('Warning:')
    #     end = text.find('\n', start)
    #     text = text[:start] + text[end:]
    #
    try:
        err = errors[e.returncode]
    except KeyError:
        err = 'error %i: Unknown Error' % e.returncode
    #
    e_stderr = e.stderr.replace('\n', '\n\n')
    #
    text = "Command:\n '%s'\n\nReturns:\n %s\n %s" % (e.cmd, e_stderr, err)
    tell(text, err)
#
#
def save_blast_output(text, fpath):
    """"""
    #
    try:
        text = text.replace('\r\n', '\n')
        open(fpath, "w").write(text)
    except IOError:
        tell('Nothing saved at %s' % fpath)
#
#
def record_from_xhandle(handle):
    """Produces a list of blast result records from a xml handle.

    Returns a list of records.
    """
    rec_generator = NCBIXML.parse(handle)
    try:
        records = [record for record in rec_generator]
    except ValueError:
        records = []
    return records
#
#
def parse_info_sequences(seq_txt):
    """Extracts sequences and data from a string.

    BlastFrame.on_blast(...)
        read input sequence files (accepts lutefisk type hits)
        build tag info&sequence string with search_mass_info(sequences)
        mutate data if needed
        save string in temp file <infile>
        then  -- calls -->
            BlastFrame.get_tags_from_blast_result(infile,...)
                reads info&sequence string <sequences> from <infile>
                then -- calls -->
                    build_tag_list(sequences,...) -- calls -->
                            make_list_query_info(seq_txt)

    The tag info&sequence string <seq_txt> (maybe generated from a lutefisk
    result file) is composed of line groups of the type:
       > Info_Entry Start# None End# None
       CARPATHIANS

    For each of this line pairs the function produces a tuple:
    ('CARPATHIANS', 'Info_Entry', 'Start# None End# None', <is_series_head>)
    <is_series_head> is a mark to combine groups of sequences generated
    by I|L mutation.

    """
    head_and_sec_list = []
    info = ""
    mass_data = ""
    is_series_head = False
    #
    lines = seq_txt.splitlines()
    for line in lines:
        if line.startswith('>'):
            is_series_head = not line.endswith('*')
            idx = line.index('Start#')
            mass_data = line[idx:]
            info = line[2:idx]
        else:
            sequence = line.strip()
            head_and_sec_list.append((sequence,
                                      info, mass_data, is_series_head))
            is_series_head = False

    return head_and_sec_list
#
#
def digest(sequence, enzyme_name='trypsin'):
    """Produces the digestion peptides from a protein sequence.

    Produces a list of the digestion peptides from 'sequence' with
    enzyme 'enzyme_name'.

    """
    #
    tuple_forbidden = Enzyme.forbidden[enzyme_name]
    for protector, item in enumerate(tuple_forbidden):
        sequence = sequence.replace(item, str(protector))
    #
    for item in Enzyme.cut[enzyme_name]:
        sequence = sequence.replace(item, item + '|')
    #
    for protector, item in enumerate(Enzyme.forbidden[enzyme_name]):
        sequence = sequence.replace(str(protector), item)
    #
    return sequence.strip().split('|')
#
#
def find_extreme_aa(head, tail, enzyme='trypsin'):
    """Find the initial and last position of enzyme target aas in two sequences.

    Search for enzyme specific breakdown sites closest to the N-term and C-term
    of sequences <head> and <tail> respectively.

            tail                       head
         ____  _____            ________  _______
    NH2-ABG K DASHES...........ASPHALT R ITERFIND-COOH
        0     4 (=tail_index)   0       7 (=head_index)
 
    """  
    head_list = digest(head, enzyme)
    tail_list = digest(tail, enzyme)
    #
    if len(head_list) == 1:
        head_index = len(head)
    else:
        head_index = len(head_list[0]) - 1

    if len(tail_list) == 1:
        tail_index = 0
    else:
        tail_index = len(tail) - len(tail_list[-1])

    return head_index, tail_index
#
#
def pass_filter(alignment, afilter):
    """Checks if the alignment title comply with the filter

    Returns False or True

    """
    items, template = atomize(afilter)
    align_title_upper = alignment.title.upper()
    formula = [item.upper() in align_title_upper for item in items]
    formula = tuple(formula)

    return not eval(template % formula)
#
#
def search_proteins(query, dbase_path):
    """Returns database records which header contains the <query> expression.
    """
    alist = []
    items, template = atomize(query)
    for line in open(dbase_path):
        if line.startswith('>'):
            line_upper = line.upper()
            formula = [item.upper() in line_upper for item in items]

            formula = tuple(formula)
            if eval(template % formula):
                alist.append(line.strip())
    #
    new_list = []
    #
    names = '|'.join(items)
    pat = recompile(names, IGNORE_CASE)

    for line in alist:
        head, body = line.split(None, 1)
        found = re_find_all(pat, body)  # re_ignore_case)
        for word in set(found):
            body = body.replace(word, '<b>' + word + '</b>')
        new_list.append(head + ' ' + body)
    return new_list
#
#
def atomize(text):
    """Dissects an expression in its variables and operators.

    Expression is a search query of the type:
        expression = '(one OR two) and three'
    The function returns a tuple (items, template) where
        items =['one','two','three']
        template = '(%s OR %s) and %s'
    If no operator is found between two items, operator AND is assumed

    """
    formula = []
    final = []
    data = []
    item = ''
    for char in text:
        if char in '()':
            formula.append(item)
            item = ''
            formula.append(char)
        else:
            item += char

    formula.append(item)

    for mol in formula:
        if mol:
            final.extend(mol.split())

    formula = []
    for atom in final:
        if atom in '()':
            formula.append(atom)
        elif atom in ['o', 'or', 'OR', 'O', '|']:
            formula.append('or')
        elif atom in ['y', 'Y', 'and', 'AND', '&']:
            formula.append('and')
        elif atom in ['not', 'NOT', 'no', 'NOT', '!']:
            formula.append('not')
        else:
            if atom.startswith('!'):    # and (len(atom) > 0):
                formula.append('not')
                atom = atom[1:]
            formula.append('%s')
            data.append(atom)
    #
    template = ' '.join(formula)

    while '%s %s' in template:
        template = template.replace('%s %s', '%s and %s')
    template = template.replace('%s (', '%s and (')
    template = template.replace(') %s', ') and %s')
    #
    return data, template
#
#
def get_title_components(alignment, dictionary, dbase_type='ncbi'):
    """Dissects the info included in the alignment title and gets the sequence.

    The alignment.title has this structure:
    # ncbi(human)
    gnl|BL_ORD_ID|6634 gi|22749237|ref|NP_689815.1| zinc finger [Homo sapiens]
                      id_0 | id_1 |id_2| id_3
    # uniprot
    gnl|BL_ORD_ID|2824 tr|C0R0X1|C0R0X1_BRA Putative uncharacterized ...
    # uniprot1:
    gnl|BL_ORD_ID|110336 Q05013|LIAM_NIMBY Capsule lipA - Neisseria meninges
                          id_0 | id_1                           names0

    """
    prot_id = ""
    prot_id2 = ""
    # noinspection PyUnusedLocal
    prot_seq = ""
    title_words = alignment.title.split()
    title_sectors = alignment.title.split('|')
    references = title_words[1]
    ids = references.split('|')
    #
    if dbase_type == "ncbi":
        # ncbi  --->  ids = [gi, 22749237, ref, NP_689815.1]
        prot_id, prot_id2 = ids[3], ids[1]
    elif dbase_type == "uniprot1":
        # uniprot1 --> ids = [Q05013, LIAM_NIMBY]
        prot_id, prot_id2 = ids[0], ids[1]
    elif dbase_type == "uniprot":
        # uniprot  --> ids = [??]
        prot_id, prot_id2 = ids[1], ids[2]
    #
    names = title_sectors[-1].split(';')
    #
    try:
        prot_seq = dictionary[prot_id][2]
    except KeyError:
        tell('Error in entry for key %s' % prot_id)
        prot_id, prot_id2 = prot_id2, prot_id
        try:
            prot_seq = dictionary[prot_id][2]
        except KeyError:
            tell('Too many errors in dictionary')
            prot_seq = ""

    return prot_id, prot_id2, names, prot_seq
#
#
def fill_match_gaps(hsp):
    """Fill gaps in blast alignments with specific char codes

     ' ' --> '&' (por gap en subject)
     ' ' --> '*' (por gap en query)

                 AESPYIPSFDLGGDFG----GGGDFAD    --> the query sequence
    hsp.query =    SPYIPSFDLGGDFG    GGGDF      --> the blast query
    hsp.match =    SPYIP   LGGDFG    GGGDF      --> the blast alignment
    hsp.sbjct =    SPYIP   LGGDFGGGDFGGGDF      --> the matched sequence

    returns   =    SPYIP&&&LGGDFG****GGGDF      --> the filled_match

    """
    match = hsp.match
    match_list = list(match)
    for index, character in enumerate(match):
        if character == ' ':
            if hsp.sbjct[index] == '-':
                match_list[index] = '&'
            elif hsp.query[index] == '-':
                match_list[index] = '*'
    return ''.join(match_list)
#
#
def give_color(patron, diana):
    """Insert HTML color codes into a sequence <diana> following a <patron>

    patron ->   $$SPYIP&&&LGGDFG****GGGDF$$
    diana  ->   ACSPYIP---LGGDFGGGDFGGGDFGG

    """
    text = ''
    code = ''
    #
    RED = '<font color="red">'
    YELL = '<font color="orange">'
    BLUE = '<font color="blue">'
    VERDE = '<font color="green">'
    BLACK = '<font color="black">'         # * jump in query
    FIN = '</font>'
    codes = {' ': YELL, '+': BLUE, '$': VERDE, '&': BLACK, '*': BLACK}
    #
    for index, char in enumerate(patron):
        code_old = code
        try:
            code = codes[char]
        except KeyError:
            code = RED
        #
        if code_old == code:
            flag = ''
        else:
            flag = FIN + code
        #
        # if there is a gap in protein (hsp.sbjct)
        try:
            letter = diana[index]
        except IndexError:
            tell('error in index = %s for %s' % (str(index), diana))
            letter = ''
        #
        text = "%s%s%s" % (text, flag, letter)

    text = text[7:]  # takes out initial tag </font>
    return '<b>' + text + FIN + '</b>'
#
#
def readfile(fpath):
    """Opens a file and returns its contents."""
    try:
        with open(fpath) as f:
            text = f.read()
    except IOError:
        text = ''
    return text


def it_is_exe(program):
    """Check whether a file is an executable."""

    def is_exe(abs_path):
        if os.path.exists(abs_path) and os.access(abs_path, os.X_OK):
            return True
        return False
    #
    if os.name == 'nt':
        program = '%s.exe' % program
    fpath, fname = os.path.split(program)
    #
    if fpath:
        return is_exe(program)
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return True

    return False
#
#
if __name__ == '__main__':

    import configparser
    
    config = configparser.ConfigParser() 
    config.read('config.commons.posix.cfg')
    BLASTPATH = config.get('directories', 'BLASTPATH', fallback='')
    
    print('***** ', BLASTPATH)
    
    testfile = 'data\\secfile'
    param = {'evalue': 500}
    expected = (
        'BLASTP 2.2.28+\n\n\nReference: Stephen F. Altschul, Thomas L. Madden, '
        'Alejandro A.\nSchaffer, Jinghui Zhang, Zheng Zhang, Webb Miller, and '
        'David J.\nLipman (1997), "Gapped BLAST and PSI-BLAST: a new generation'
        ' of\nprotein database search programs", Nucleic Acids Res. 25:3389-'
        '3402.\n\n\nReference for composition-based statistics: Alejandro A. '
        'Schaffer,\nL. Aravind, Thomas L. Madden, Sergei Shavirin, John L. '
        'Spouge, Yuri\nI. Wolf, Eugene V. Koonin, and Stephen F. Altschul '
        '(2001),\n"Improving the accuracy of PSI-BLAST protein database '
        'searches with\ncomposition-based statistics and other refinements", '
        'Nucleic Acids\nRes. 29:2994-3005.\n\n\n\nDatabase: C:/Blast/DBs\\uni'
        'prot_sp\n           362,782 sequences; 130,497,792 total letters\n\n\n'
        '\nQuery= \nLength=15\n                                             '
        '                         Score     E\nSequences producing significant'
        ' alignments:                          (Bits)  Value\n\n  Q9FL28|FLS2_'
        'ARATH LRR receptor-like serine/threonine-protein ki...  20.0      413'
        '\n\n\n> Q9FL28|FLS2_ARATH LRR receptor-like serine/threonine-protein '
        '\nkinase FLS2 precursor - Arabidopsis thaliana (Mouse-ear cress)'
        '\nLength=1173\n\n Score = 20.0 bits (40),  Expect =   413, Method: '
        'Composition-based stats.\n Identities = 8/14 (57%), Positives = 9/14 '
        '(64%), Gaps = 0/14 (0%)\n\nQuery  1    ASDLFGRTDFIASR  14\n          '
        '  ASDL G TD   S+\nSbjct  773  ASDLMGNTDLCGSK  786\n\n\n\nLambda      K'
        '        H        a         alpha\n   0.331    0.143    0.398    0.792 '
        '    4.96 \n\nGapped\nLambda      K        H        a         alpha    '
        'sigma\n   0.267   0.0410    0.140     1.90     42.6     43.6 \n\n'
        'Effective search space used: 1957466880\n\n\n  Database: C:/Blast/'
        'DBs\\uniprot_sp\n    Posted date:  Aug 17, 2011 10:18 AM\n  Number of '
        'letters in database: 130,497,792\n  Number of sequences in database:  '
        '362,782\n\n\n\nMatrix: BLOSUM62\nGap Penalties: Existence: 11, '
        'Extension: 1\nNeighboring words threshold: 11\nWindow for multiple '
        'hits: 40\n', ''
    )

    result = blast(testfile, blastpath=BLASTPATH, kwargs=param)
    
    if expected == result:
        print('**** OUTPUT IDENTICAL TO EXPECTED *********\n\n')
    else:
        print('**** OUTPUT DIFFERENT TO EXPECTED *********\n\n')

    result = result[0]
    result = result.replace('\n\n', '\n')
    print(result)
