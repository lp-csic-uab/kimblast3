#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
#
"""
kblast_combinator
"""
#
# XXXLXLXXTXXXLXXX    a peptide string produced by PEAKS
#    L L  L   L       with series of ambiguous Leucines
#
#    LLLL             for which all L/I combinations are calculated
#    LLLI
#    LLIL
#    -----
# Each L in the sequence is reassigned
# and that is!
#
twin_codes = "LI"
#
#
def combine(length, codes=twin_codes):
    """Produces all the combinations of length <length> of letters in <codes>.
    """
    alist = [""]
    for pos in range(length):
        alist = [item + aa for aa in codes for item in alist]

    return alist
#
#
def insert(core, combinations, codes=twin_codes):
    """Genera un conjunto de secuencias a partir de la secuencia original <core>
    donde se substituyen los aminoacidos LI <twin_codes> originales por cada una
    de las combinaciones <combinations> calculadas para estos aminoacidos
    """
    full = []
    indexes = range(len(core))
    new = list(core)
    #
    for comb in combinations:
        pos = 0
        for index in indexes:
            if new[index] in codes:
                new[index] = comb[pos]
                pos += 1
        full.append(''.join(new))
    return full
#
#
def mutate_peptide(seq, codes=twin_codes):
    """"""
    largo = 0
    for aa in codes:
        largo += seq.count(aa)
    combinations = combine(largo)
    return insert(seq, combinations)
#
#
def mutate_entries(sequences, codes=twin_codes):
    """"""
    new_seqs = []
    head_asterisk = ''
    # head = "No head"
    seqs = sequences.split("\n")
    for line in seqs:
        if line.startswith('>'):
            # head = line
            head_asterisk = line + '*'
            new_seqs.append(line)
            continue
        else:
            if codes_in_seq(codes, line):
                mutants = mutate_peptide(line.strip())
                new_mutants = []
                start = True
                for item in mutants:
                    if start:
                        new_mutants.append(item)
                        start = False
                    else:
                        new_mutants.append(head_asterisk)
                        new_mutants.append(item)

                new_seqs.extend(new_mutants)
            else:
                new_seqs.append(line)

    return '\n'.join(new_seqs)
#
#
def codes_in_seq(codes, seq):
    """Check if code belongs to sequence.
    """
    return any(aa in seq for aa in codes)
#  
#
#
if __name__ == "__main__":

    peptide = "ABCLALBL"
    counts = peptide.count('L') + peptide.count('I')
    combinations = combine(counts)
    print("   * * * Test 1 * * *")
    print(insert(peptide, combinations))
    print("   * * * Test 2 * * *")
    three_sequences = "> first\nABCLAL\n> second\nASFLGRT\n> third\nASFGRT"
    print(mutate_entries(three_sequences))


#   * * * Test 1 * * *
# ['ABCLALBL', 'ABCIALBL', 'ABCLAIBL', 'ABCIAIBL', 'ABCLALBI', 'ABCIALBI', 'ABCLAIBI', 'ABCIAIBI']
#   * * * Test 2 * * *
# ['ABCLAL', 'ABCIAL', 'ABCLAI', 'ABCIAI']
# ['ASFLGRT', 'ASFIGRT']
# > first
# ABCLAL
# > first*
# ABCIAL
# > first*
# ABCLAI
# > first*
# ABCIAI
# > second
# ASFLGRT
# > second*
# ASFIGRT
# > third
# ASFGRT
# Script terminated.
