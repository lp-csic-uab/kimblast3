#!/usr/bin/env python
# -*- coding: iso-8859-15 -*-
# generated by wxGlade HG on Sat Sep 24 10:51:56 2011

import wx

# begin wxGlade: extracode
# end wxGlade

class QueryBasePanel(wx.Panel):
    def __init__(self, *args, **kwds):
        # begin wxGlade: QueryBasePanel.__init__
        kwds["style"] = wx.TAB_TRAVERSAL
        wx.Panel.__init__(self, *args, **kwds)
        self.tc_query = wx.TextCtrl(self, -1, "", style=wx.TE_MULTILINE)
        self.bt_save = wx.Button(self, -1, "Save")
        self.bt_fix = wx.Button(self, -1, "Fix")
        self.bt_clr = wx.Button(self, -1, "Clear")
        self.pn_2 = wx.Panel(self, -1, style=wx.RAISED_BORDER)
        self.ck_mutate = wx.CheckBox(self.pn_2, -1, "mutate")
        self.ck_join = wx.CheckBox(self.pn_2, -1, "join result")
        self.szr_staticbox = wx.StaticBox(self.pn_2, -1, "Ile | Leu")
        self.bt_blast = wx.Button(self.pn_2, -1, "BLAST")
        self.ck_filter = wx.CheckBox(self.pn_2, -1, "text filter")

        self.__set_properties()
        self.__do_layout()

        self.Bind(wx.EVT_BUTTON, self.on_fix, self.bt_fix)
        self.Bind(wx.EVT_BUTTON, self.on_clear, self.bt_clr)
        # end wxGlade

    def __set_properties(self):
        # begin wxGlade: QueryBasePanel.__set_properties
        self.SetSize((682, 75))
        self.bt_save.SetMinSize((45, 22))
        self.bt_fix.SetMinSize((45, 22))
        self.bt_clr.SetMinSize((45, 22))
        self.bt_blast.SetMinSize((85, 35))
        self.pn_2.SetMinSize((220, -1))
        self.pn_2.SetBackgroundColour(wx.Colour(255, 244, 148))
        # end wxGlade

    # noinspection PyArgumentList
    def __do_layout(self):
        # begin wxGlade: QueryBasePanel.__do_layout
        szr_1 = wx.BoxSizer(wx.HORIZONTAL)
        szr_3 = wx.BoxSizer(wx.HORIZONTAL)
        szr_5 = wx.BoxSizer(wx.VERTICAL)
        self.szr_staticbox.Lower()
        szr_4 = wx.StaticBoxSizer(self.szr_staticbox, wx.VERTICAL)
        szr_2 = wx.BoxSizer(wx.VERTICAL)
        szr_1.Add(self.tc_query, 1, wx.EXPAND | wx.LEFT | wx.UP, 3)
        szr_2.Add(self.bt_save, 0, wx.BOTTOM, 2)
        szr_2.Add(self.bt_fix, 0, wx.BOTTOM, 2)
        szr_2.Add(self.bt_clr, 0, 0, 0)
        szr_1.Add(szr_2, 0, wx.RIGHT | wx.ALIGN_CENTER_VERTICAL, 10)
        szr_4.Add(self.ck_mutate, 0, wx.TOP, 2)
        szr_4.Add(self.ck_join, 0, wx.TOP, 2)
        szr_3.Add(szr_4, 0, wx.LEFT | wx.RIGHT, 15)
        szr_5.Add(self.bt_blast, 0,
                  wx.TOP | wx.ALIGN_CENTER_HORIZONTAL, 2)
        szr_5.Add(self.ck_filter, 0, wx.TOP, 5)
        szr_3.Add(szr_5, 0, wx.LEFT | wx.EXPAND, 4)
        self.pn_2.SetSizer(szr_3)
        szr_1.Add(self.pn_2, 0, wx.RIGHT | wx.ALIGN_CENTER_VERTICAL, 5)
        self.SetSizer(szr_1)
        # end wxGlade

    def on_fix(self, event):  # wxGlade: QueryBasePanel.<event_handler>
        print("Event handler `on_fix' not implemented!")
        event.Skip()

    def on_clear(self, event):  # wxGlade: QueryBasePanel.<event_handler>
        print("Event handler `on_clear' not implemented!")
        event.Skip()

# end of class QueryBasePanel
