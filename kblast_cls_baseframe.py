#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
#
"""
kblast_cls_baseframe
"""
#
import os
import wx
from commons.iconic import Iconic
from commons.warns import tell
import kblast_common as kbc
from kblast_dialog import Colors, About
from kblast_cls_filepanel import FilePanel
from kblast_class_query_base_panel import QueryBasePanel
from kblast_ico import ICON
from kblast_funcs import readfile
from kblast_dialog import open_file
from configparser import ConfigParser, NoSectionError, NoOptionError
from wx.html2 import WebView, EVT_WEBVIEW_ERROR, EVT_WEBVIEW_NAVIGATING
#
if os.name == 'posix':
    from commons.mcalc_gtk import MiniCalc
    SIZE = (330, 95)
else:
    from commons.mcalc import MiniCalc
    SIZE = (280, 95)
#
#
# noinspection PyClassHasNoInit
class MyParams:
    """Hints and selection options"""
    COMMAND = "blast: sequence->protein database"
    OUTFMT = """-m  alignment view options:
                0 = pairwise (Default),
                1 = query-anchored showing identities,
                2 = query-anchored no identities,
                3 = flat query-anchored, show identities,
                4 = flat query-anchored, no identities,
                5 = XML Blast output,
                6 = tabular,
                7 = tabular with comment lines,
                8 = Text ASN.1,
                9 = Binary ASN.1,
                10 = Comma-separated values,
                11 = BLAST archive format (ASN.1)"""
    GAPPED = 'Allow gaps (T/F). No for TBLASTX'
    BLASTYPE = ("blastp: protein<->protein database\n"
                "blastn: nucleotide<->protein database\n"
                "blastx: nucleotide.6-frame<->protein.database\n"
                "tblastn:protein<->nucleotide.dbase.6-frame\n"
                "tblastx: nucleotide.6-frame<->nucleotide dbase.6-frame")
    FORMAT = ("html, xml, txt -> conventional blast format outputs.\n"
              "custom -> nicer (default)\n"
              "custom_bw -> no colors in sequences")
    GAPOPEN = "Cost to open a gap (zero invokes default behavior) default = 0"
    MATRIX = ''
    ENZYME = 'Enzyme used for virtual digestion in custom mode. Default=Trypsin'
    GAPEXTEND = ("Minimum word score such that the word is added"
                 "to the BLAST lookup table (real > 0)")
    DBASE = 'Select NCBI and Uniprot databases'
    ALIGNMENTS = 'Max number of alignments to show. Default=10'
    SEG = 'Filter query sequence with SEG. default = no'
    EVALUE = ("number of database hits you expect to find by chance\n"
              "Increase to get more matches of lower score")
    DESCRIPTIONS = ("Number of database sequences to show\n"
                    " one-line descriptions for (default=500)")
    WORD_SIZE = ("if default (0):  \nblastn=11  (min 7)\n"
                 "megablast=28 (min 8)\nothers=3  (min 2)")
    #
    # It is important to use a list because it can grow and its value
    # reflected in dictionary 'variables'
    # to eliminate dbases you must do 'remove',
    # if dbase_fnames = [DBASE] is reassigned to do the clean-up,
    # then the reference in 'variables' is lost.
    dbase_fnames = ['dbase', DBASE]
    #
    blast_params = {
        'command':  ['command', COMMAND, 'blast', 'blast'],
        'outfmt':   ['outfmt', OUTFMT,
                     'pairwise', 'pairwise', 'q.a.n.i',
                     'f.q.a.i', 'f.q.a.n.i',
                     'xml', 'tab', 'tab.cl',
                     'asn.txt', 'asn.bin',
                     'csv', 'asn.blast'],
        'ungapped': ['ungapped', GAPPED, 'False', 'True', 'False'],
        'blastype': ['blastype', BLASTYPE,
                     'blastp', 'blastp', 'blastx',
                     'blastn', 'tblastx', 'tblastn'],
        'format':   ['format', FORMAT,
                     'custom', 'html', 'txt',
                     'xml', 'custom', 'custom_bw'],
        'gapopen':  ['gapopen', GAPOPEN,
                     '10', '32767', '10', '9', '8', '7', '6', '5'],
        'matrix':   ['matrix', MATRIX,
                     'PAM30', 'PAM30', 'PAM70',
                     'BLOSUM45', 'BLOSUM62', 'BLOSUM80'],
        'enzyme':   ['enzyme', ENZYME,
                     'trypsin', 'trypsin', 'endolys', 'glu_c', 'none'],
        'gapextend': ['gapextend', GAPEXTEND, '1', '32767', '1', '2'],
        'db': dbase_fnames,
        'num_alignments': ['aligns', ALIGNMENTS,
                           '10', '5', '10', '20', '50', '100', '250'],
        'seg':    ['seg', SEG, 'no', 'no', 'yes', 'window locut hicut'],
        'evalue': ['evalue', EVALUE,
                   '5000', '0.01', '0.1', '1', '10',
                   '100', '1000', '5000', '10000'],
        'num_descriptions': ['descripts', DESCRIPTIONS,
                             '10', '0', '5', '10', '20', '50', '100', '250'],
        'word_size': ['word_size', WORD_SIZE, '2', '0', '2', '3', '11', '28'],
    }
    #
    shorted_selectors = ['command',  'outfmt',           'ungapped',
                         'blastype', 'format',           'gapopen',
                         'matrix',   'enzyme',           'gapextend',
                         'db',       'num_alignments',   'seg',
                         'evalue',   'num_descriptions', 'word_size']
    #
    cip = ConfigParser()
    cip.read('config_init.cfg')
    for selector_name in shorted_selectors:
        if cip.has_option('blastp-s', selector_name):
            alias = cip.get('translations', selector_name)
            blast_params[selector_name][0] = alias

            TIP = cip.get('tips', selector_name)
            if TIP:
                blast_params[selector_name][1] = TIP

            value = cip.get('blastp-s', selector_name)
            values = value.split('|')
            if values:
                blast_params[selector_name][2:] = values
    #
    # NCBI-BLAST provides several combinations of scoring matrices and gap cost.
    # These are the only combinations of gap penalties that can be used with
    # a given matrix
    # - https://doctorlib.info/medical/blast/18.html
    # - Ian Korf, Mark Yandell and Joseph Bedell in BLAST, 2003, Appendix C
    #   ISBN 978-0-596-00299-2
    #
    # tuples (gap-open, gap-extend)
    # Define the value of one of them relative to the value of the other
    # The first tuple correspond to the default values for each matrix.
    #
    gap_values = {
            'PAM30':    [(9, 1), (32767, 32767), (7, 2), (6, 2),
                         (5, 2), (10, 1), (8, 1)],
            'PAM70':    [(10, 1), (32767, 32767), (8, 2), (7, 2),
                         (6, 2), (11, 1), (9, 1)],
            'BLOSUM45': [(14, 2), (32767, 32767), (13, 3), (12, 3),
                         (11, 3), (10, 3), (16, 2), (15, 2),
                         (13, 2), (12, 2), (19, 1),
                         (18, 1), (17, 1), (16, 1)],
            'BLOSUM62': [(11, 1), (32767, 32767), (11, 2), (10, 2),
                         (8, 2), (7, 2), (6, 2), (13, 1), (12, 1),
                         (9, 2), (10, 1), (9, 1)],
            'BLOSUM80': [(10, 1), (32767, 32767), (25, 2), (13, 2), (9, 2),
                         (8, 2), (7, 2), (6, 2), (11, 1), (9, 1)]
    }
#
#
#
# noinspection PyUnusedLocal,PyArgumentList,PyPep8Naming
class QueryPanel(QueryBasePanel):
    def __init__(self, parent, ID=-1, pos=wx.DefaultPosition, size=(-1, 70)):
        QueryBasePanel.__init__(self, parent, ID, pos, size, style=wx.RAISED_BORDER)
        self.SetMinSize(size)

        tooltips = {'QUERY': ('Sequence(s) to be blasted\n'
                              'write | copy-paste and save'
                              ' or load Sequence file'),
                    'SAVE':  'Save sequence(s) in current Seq file',
                    'FIX':   'Clean text from non-amino acid characters',
                    'CLEAR': 'Clear window',
                    'MUTATE': ('Consider Ile and Leu as the same amino acid\n'
                               '(All possible I/L combinations will be'
                               ' Blasted.)'),
                    'JOIN':  'Combine and short results of the different'
                             ' I/L combinations',
                    'BLAST': 'Blast !',
                    'FILTER': ('Select only BLAST matches containing\n'
                               'the string in the Text Search window')
                    }
        self.tc_query.SetToolTip(tooltips['QUERY'])
        self.bt_save.SetToolTip(tooltips['SAVE'])
        self.bt_fix.SetToolTip(tooltips['FIX'])
        self.bt_clr.SetToolTip(tooltips['CLEAR'])
        self.ck_mutate.SetToolTip(tooltips['MUTATE'])
        self.ck_join.SetToolTip(tooltips['JOIN'])
        self.bt_blast.SetToolTip(tooltips['BLAST'])
        self.ck_filter.SetToolTip(tooltips['FILTER'])

    def on_clear(self, evt):
        self.tc_query.Clear()

    def on_fix(self, evt):
        """Remove unexpected chars or spaces from a sequence.

        Do not alter headers '> ...etc'.
        """
        text_list = []
        amino_acids = 'QWERTYIPASDFGHKLMNVCX'
        text = self.tc_query.GetValue()
        lines = text.split('\n')

        for line in lines:
            if line.startswith('>'):
                text_list.append('\n' + line + '\n')
            else:
                linea = [item for item in line if item in amino_acids]
                line = "".join(linea)
                text_list.append(line)

        final_txt = ''.join(text_list)

        self.tc_query.Clear()
        final_txt = final_txt.strip()
        self.tc_query.SetValue(final_txt)
#
#
# noinspection PyArgumentList,PyUnusedLocal,PyPep8Naming
class BlastParamSelectorPanel(wx.Panel):
    def __init__(self, parent, ID=-1, pos=wx.DefaultPosition, size=(454, 105)):
        wx.Panel.__init__(self, parent, ID, pos, size)
        name_size = (61, 20)
        cbbox_size = (84, 20)
        self.blast_type = None
        self.SetBackgroundColour("yellow")
        #
        if os.name == 'posix':
            f = self.GetFont()
            f.SetPointSize(8)
            self.SetFont(f)
        #
        self.SetMinSize(size)
        self.sizer = wx.FlexGridSizer(rows=5, cols=6, hgap=3, vgap=1)
        self.param_selector_cbboxes = {}
        self.build_param_selector(cbbox_size, name_size)
        self.fill_all_blastype(None)
        #
        self.Bind(wx.EVT_COMBOBOX,
                  self.fill_all_blastype, self.param_selector_cbboxes['blastype'])
        #
        self.Bind(wx.EVT_COMBOBOX,
                  self.fill_gaps, self.param_selector_cbboxes['matrix'])
        self.Bind(wx.EVT_COMBOBOX,
                  self.fill_gap_by_ext, self.param_selector_cbboxes['gapextend'])
        self.Bind(wx.EVT_COMBOBOX,
                  self.fill_ext_by_gap, self.param_selector_cbboxes['gapopen'])
        #
        self.SetSizer(self.sizer)
        self.Fit()

    def fill_all_blastype(self, evt):
        """Set default values in param windows on the basis of the blastype.

        Take the values from those set in the corresponding configuration file
        (p.e. in params/blastp_params.cfg).

        """

        btp = self.param_selector_cbboxes['blastype'].GetValue()
        if btp == self.blast_type:
            return
        #
        kbc.config.remove_section('blast type options')
        kbc.config.read(os.path.join('params', '%s_params.cfg' % btp))
        #
        for cbox in self.param_selector_cbboxes:
            try:
                value = kbc.config.get('blast type options', cbox)
                self.param_selector_cbboxes[cbox].SetValue(value)
            except (NoSectionError, NoOptionError):
                pass

        outfmt = self.param_selector_cbboxes['outfmt'].GetValue()
        self.param_selector_cbboxes['outfmt'].\
            SetValue(MyParams.blast_params['outfmt'][3 + int(outfmt)])

    # noinspection PyUnresolvedReferences
    def fill_gaps(self, evt):
        """Fill the values for cbboxes gap open and gap extend on the basis of
        the allowed gap_values for each BLAST matrix.
        Select the default pair of values recommended for each matrix

        """
        matrix = self.param_selector_cbboxes['matrix'].GetValue()
        gaps = MyParams.gap_values[matrix]

        cbbox_gapopen = self.param_selector_cbboxes['gapopen']
        cbbox_gapextend = self.param_selector_cbboxes['gapextend']

        cbbox_gapopen.SetItems(sorted([str(gap[0]) for gap in gaps]))
        cbbox_gapextend.SetItems(sorted([str(gap[1]) for gap in gaps]))

        # 1st tuple in gaps are the default gap open and gap extend for the matrix
        cbbox_gapopen.SetValue(str(gaps[0][0]))
        cbbox_gapextend.SetValue(str(gaps[0][1]))

    def fill_gap_by_ext(self, evt):
        """Set the allowed value of gap open when changing gap extend.

        """
        matrix = self.param_selector_cbboxes['matrix'].GetValue()
        values = MyParams.gap_values[matrix]
        widget_gapopen = self.param_selector_cbboxes['gapopen']
        gap_extend = int(self.param_selector_cbboxes['gapextend'].GetValue())
        gapopen = int(self.param_selector_cbboxes['gapopen'].GetValue())

        opens = { x[0] for x in values if x[1] == gap_extend }

        if gapopen not in opens:
            diff = [(abs(x - gapopen), x) for x in opens]
            value = min(diff)
            widget_gapopen.SetValue(str(value[1]))

    def fill_ext_by_gap(self, evt):
        """Set the allowed value of gap extend when changing gap open.

        """
        matrix = self.param_selector_cbboxes['matrix'].GetValue()
        values = MyParams.gap_values[matrix]
        widget_gap_extend = self.param_selector_cbboxes['gapextend']
        gap_extend = int(self.param_selector_cbboxes['gapextend'].GetValue())
        gapopen = int(self.param_selector_cbboxes['gapopen'].GetValue())

        exts = { x[1] for x in values if x[0] == gapopen }

        if gap_extend not in exts:
            diff = [(abs(x - gap_extend), x) for x in exts]
            value = min(diff)
            widget_gap_extend.SetValue(str(value[1]))

    def build_param_selector(self, num, let):
        """
        """
        flag1 = wx.ALIGN_LEFT
        flag3 = wx.ALIGN_CENTER

        bparams = MyParams.blast_params
        selector_names = MyParams.shorted_selectors
        for param_name in selector_names:
            param_values = bparams[param_name]
            self.sizer.Add(wx.StaticText(self, label=' ' + param_values[0],
                                         size=let, style=flag1))
            #
            cbox = wx.ComboBox(self, -1, param_values[2],
                               choices=param_values[3:],
                               size=num, style=wx.CB_DROPDOWN)

            cbox.SetToolTip(param_values[1])
            self.sizer.Add(cbox, 1, flag3)
            self.param_selector_cbboxes[param_name] = cbox

        self.blast_type = self.param_selector_cbboxes['blastype']

    def refresh_dbase_choices(self, choices, default=''):
        """"""
        # DICT_DTBS --> human_p:human.aa,human.psq,human_p.idx
        lchoices = list(choices)
        if default not in lchoices:
            default = lchoices[0]

        self.param_selector_cbboxes['db'].SetItems(lchoices)
        self.param_selector_cbboxes['db'].SetValue(default)
#
#
# noinspection PyArgumentList,PyPep8Naming
class Calculator(wx.Panel):
    def __init__(self, parent, ID=-1):
        wx.Panel.__init__(self, parent, ID, style=wx.RAISED_BORDER)
        self.properties = {}
        self.hsizer = wx.BoxSizer(wx.HORIZONTAL)
        self.vsizer = wx.BoxSizer(wx.VERTICAL)
        
        self.calc = MiniCalc(self, size=SIZE)
        if os.name == 'nt':
            self.calc.bt_clone.Destroy()
        # self.calc.separator = wx.Size(0,0)
        self.build_checkboxes()
        self.hsizer.Add(self.calc, 0)
        self.hsizer.Add(self.vsizer, 0)
        self.SetSizer(self.hsizer)
        self.SetBackgroundColour(wx.Colour(200, 255, 100))   
        self.Fit()

    def build_checkboxes(self):
        flag = wx.LEFT | wx.RIGHT
        border = 5
        #
        self.vsizer.Add(wx.StaticText(self, -1, 'fixed mods'), 0, flag, border)
        self.vsizer.Add(wx.StaticLine(self, -1), 1, flag | wx.EXPAND, border)
        #
        checkboxes = [('carbamidomethylated cysteine', 'C-cam'),
                      ('oxidized methionine (sulphoxide)', 'M-ox')]
        #
        for eachTip, eachLabel in checkboxes:
            property_ = wx.CheckBox(self, -1,
                                    eachLabel, size=(75, 24), style=flag)
            self.vsizer.Add(property_, 0, flag, border)
            property_.SetToolTip(eachTip)
            self.properties[eachLabel] = property_
#
#
# noinspection PyArgumentList,PyPep8Naming
class TaskButtons(wx.Panel):
    def __init__(self, parent, ID=-1, size=(100, 50)):
        wx.Panel.__init__(self, parent, ID, size)
        self.properties = {}
        self.sizer = wx.BoxSizer(wx.VERTICAL)
        self.make_buttons()
        self.SetSizer(self.sizer)
        self.Fit()

    def make_buttons(self):
        SETUP = ("1.- Format fasta files with makeblastdb for BLAST searches\n"
                 "2.- Indexes fasta files with Biopython index\n\n"
                 "Protein or nucleotide Fasta files MUST be formatted before\n"
                 "these databases can be searched by blastall, blastpgp or\n"
                 "MegaBLAST.\n\n"
                 "Indexation is needed for fast data search of the fasta file")

        COLOR_KEY = """Color keys for sequence homology matches"""
        ABOUT = 'ABOUT'
        flag1 = wx.ALIGN_CENTER_HORIZONTAL | wx.TOP
        flag2 = wx.ALIGN_CENTER_VERTICAL
        buttons = [(SETUP, 'Setup Fasta'),
                   (COLOR_KEY, 'Color keys'), (ABOUT, 'About')]
        for eachTip, eachLabel in buttons:
            property_ = wx.Button(self, -1,
                                  eachLabel, size=(80, 23), style=flag2)
            self.sizer.Add(property_, 0, flag1, 1)
            property_.SetToolTip(eachTip)
            self.properties[eachLabel] = property_
#
#
# noinspection PyArgumentList,PyPep8Naming
class ParamPanel(wx.Panel):
    def __init__(self, parent, ID=-1, pos=wx.DefaultPosition, size=(400, 115)):
        wx.Panel.__init__(self, parent, ID, pos, size, wx.RAISED_BORDER)
        self.SetBackgroundColour("yellow")
        self.SetMinSize(size)
        self.sizer = wx.BoxSizer(wx.HORIZONTAL)
        #
        self.sizer.Add(wx.Panel(self, -1, size=(6, 10)))
        self.wleft = BlastParamSelectorPanel(self)
        self.wcenter = Calculator(self)
        self.wright = TaskButtons(self)
        #
        self.sizer.Add(self.wleft, 0, wx.CENTER)
        self.sizer.Add(wx.Panel(self, -1, size=(10, 10)))
        self.sizer.Add(self.wcenter, 0, wx.CENTER)
        self.sizer.Add(wx.Panel(self, -1, size=(7, 10)))
        self.sizer.Add(self.wright, 1, wx.TOP, 10)
        self.sizer.AddSpacer(15)
        self.SetSizer(self.sizer)
        self.Fit()
#
#
# noinspection PyArgumentList,PyUnusedLocal
# noinspection PyArgumentEqualDefault,PyMethodMayBeStatic
# noinspection PyPep8Naming
class HtmlPanel(wx.Panel):
    def __init__(self, parent, ID=-1, pos=wx.DefaultPosition, size=(300, 325)):
        wx.Panel.__init__(self, parent, ID, pos, size, wx.RAISED_BORDER)
        #
        self.current = "https://www.uniprot.org/"
        self.lines = []
        button_size = (45, 25)
        buttons = [("<--", self.on_prev_page),
                   ("-->", self.on_next_page),
                   ("Stop", self.on_stop),
                   ("Refresh", self.on_refresh),
                   ("Home", self.on_home),
                   ("Open", self.on_open)
                   ]
        #
        URLs = ["http://www.biopython.org/", "http://130.14.29.110/BLAST/",
                "http://www.expasy.org", "http://www.python.org/"
                ]
        #
        sizer = wx.BoxSizer(wx.VERTICAL)
        buttons_sizer = wx.BoxSizer(wx.HORIZONTAL)
        #
        for button, func in buttons:
            widget = wx.Button(self, -1, button, size=button_size)
            self.Bind(wx.EVT_BUTTON, func, widget)
            buttons_sizer.Add(widget, 0, wx.ALL, 1)
        #
        self.log = wx.TextCtrl(self, -1, size=(-1, 35), style=wx.TE_MULTILINE)
        f = wx.Font(8, wx.FONTFAMILY_DEFAULT,
                    wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_NORMAL)
        self.log.SetFont(f)
        self.log.SetBackgroundColour((230, 230, 230))
        #
        txt = wx.StaticText(self, -1, " Location: ", size=(-1, 20),
                            style=wx.ALIGN_RIGHT | wx.ALIGN_CENTER_VERTICAL)
        # location size determine the initial width of the frame
        self.location = wx.ComboBox(self, -1, "", choices=URLs, size=(200, 23),
                                    style=wx.CB_DROPDOWN | wx.TE_PROCESS_ENTER)

        bsw = wx.Button(self, -1, 'Save', size=button_size)
        self.bs_xcl = wx.Button(self, -1, 'ToExcel', size=button_size)
        self.bt_google = wx.Button(self, -1, 'Google', size=button_size)
        self.bt_hvt = wx.Button(self, -1, 'String', size=button_size)
        #
        self.bt_increase = wx.Button(self, -1, "A", size=(35, 25))
        self.bt_decrease = wx.Button(self, -1, "A", size=(35, 25))
        self.bt_increase.SetFont(wx.Font(10, wx.FONTFAMILY_DEFAULT,
                                         wx.FONTSTYLE_NORMAL,
                                         wx.FONTWEIGHT_BOLD, 0, ""))
        self.bt_decrease.SetFont(wx.Font(6, wx.FONTFAMILY_DEFAULT,
                                         wx.FONTSTYLE_NORMAL,
                                         wx.FONTWEIGHT_NORMAL, 0, ""))
        #
        self.web_view = WebView.New(self, -1, url='https://www.google.es',
                                    style=wx.NO_FULL_REPAINT_ON_RESIZE)
        #
        bsw.SetToolTip('Save current local html document')
        self.bs_xcl.SetToolTip('Export Blast Results View to excel')
        self.bt_google.SetToolTip('Search Google for Selected string')
        self.bt_hvt.SetToolTip('Search STRING for selected protein name\n'
                               'Press repeatedly to decrease search score'
                               )
        #
        buttons_sizer.Add(txt, 0, wx.CENTER | wx.ALL, 1)
        buttons_sizer.Add(self.location, 10, wx.ALL, 2)
        buttons_sizer.Add(bsw, 0, wx.EXPAND | wx.ALL, 1)
        buttons_sizer.Add(self.bs_xcl, 0, wx.ALL, 1)
        buttons_sizer.Add(self.bt_google, 0, wx.ALL, 1)
        buttons_sizer.Add(self.bt_hvt, 0, wx.ALL, 1)
        buttons_sizer.Add((10, 10))
        buttons_sizer.Add(self.bt_increase, 0, wx.TOP | wx.BOTTOM, 1)
        buttons_sizer.Add(self.bt_decrease, 0, wx.TOP | wx.BOTTOM | wx.RIGHT, 1)
        #
        sizer.Add(buttons_sizer, 0, wx.EXPAND)
        sizer.Add(self.web_view, 1, wx.EXPAND)
        sizer.Add(self.log, 0, wx.EXPAND)
        #
        self.web_view.Bind(EVT_WEBVIEW_ERROR, self.NavigateError)
        self.location.Bind(wx.EVT_KEY_UP, self.OnLocationKey)
        self.location.Bind(wx.EVT_CHAR, self.IgnoreReturn)
        self.Bind(wx.EVT_COMBOBOX, self.OnLocationSelect, self.location)
        self.Bind(wx.EVT_BUTTON, self.on_save_win, bsw)
        self.Bind(wx.EVT_SIZE, self.on_size)
        #
        self.SetSizer(sizer)
        #
        self.URLs = URLs
        self.first_error = True
        self.web_view.LoadURL(self.current)
        self.location.SetValue(self.current)

    def on_size(self, evt):
        self.Layout()

    def OnLocationSelect(self, evt):
        url = self.location.GetStringSelection()
        self.log_text('OnLocationSelect: %s\n' % url)
        self.web_view.LoadURL(url)

    def OnLocationKey(self, evt):
        """"""
        if evt.KeyCode == wx.WXK_RETURN:
            URL = self.location.GetValue()
            self.location.Append(URL)
            self.web_view.LoadURL(URL)
        else:
            evt.Skip()

    def IgnoreReturn(self, evt):
        if evt.GetKeyCode() != wx.WXK_RETURN:
            evt.Skip()

    def on_open(self, event):
        dlg = wx.FileDialog(self, message="Choose a file to Open...",
                            defaultDir=os.getcwd(), defaultFile="",
                            wildcard=kbc.WILDCARD,
                            style=wx.FD_OPEN | wx.FD_CHANGE_DIR)
        #
        if dlg.ShowModal() == wx.ID_OK:
            fpath = dlg.GetPath()
            self.current = 'file:///%s' % fpath
            self.web_view.LoadURL(self.current)
            self.location.SetValue(self.current)
        #
        dlg.Destroy()

    def on_home(self, event):
        url = 'http://proteomica.uab.cat'
        self.web_view.LoadURL(url)
        self.location.SetValue(url)

    def on_prev_page(self, event):
        if self.web_view.CanGoBack():
            self.web_view.GoBack()

    def on_next_page(self, event):
        if self.web_view.CanGoForward():
            self.web_view.GoForward()

    def on_stop(self, evt):
        self.web_view.Stop()

    def on_refresh(self, evt):
        self.web_view.Reload()

    def on_save_win(self, evt):
        """
        Save/Copy results file
        """
        import shutil
        #
        if 'posix' not in os.name:
            start = 8
        else:
            start = 7
        # noinspection PyUnresolvedReferences
        url = self.web_view.GetCurrentURL()
        # noinspection PyUnresolvedReferences
        if url.startswith('file:'):
            source_path = url[start:]
        else:
            tell('Only local files can be saved!')
            return
        #
        target_path, _ = open_file(self.location)
        if not target_path:
            return
        #
        # noinspection PyProtectedMember
        if shutil._samefile(source_path, target_path):
            tell('Source and target filenames are the same! Nothing done.')
            return
        #
        shutil.copyfile(source_path, target_path)
        self.location.Append(target_path)

    def on_search_string(self, evt):
        """"""
        evt.Skip()

    def on_save_xls(self, evt):
        """"""
        evt.Skip()

    def logEvt(self, evt):
        pst = ""
        for name in evt.paramList:
            pst += " %s:%s " % (name, repr(getattr(evt, name)))

        self.log_text('%s: %s' % (evt.eventName, pst))

    def log_text(self, text, colored=False):
        """Writes a text in the status TexControl"""
        if len(text) > 200 and text.startswith('Document Complete'):
            text = text[:100] + '...'
        self.lines.append('- %s' % text.strip())
        if len(self.lines) > 5:
            self.lines.pop(0)
        self.log.Clear()
        # orange else white
        ink = (254, 229, 184) if colored else (230, 230, 230)
        self.log.SetBackgroundColour(ink)
        self.log.AppendText('\n'.join(self.lines))

    def DocumentComplete(self, this, pDisp, URL):
        """
        Method name that match a COM Event name
        """
        self.current = str(URL[0])
        self.location.SetValue(self.current)
        self.log_text('Document Complete: %s' % self.current)

    def NavigateError(self, evt):
        """
        What to do if page load fails
        """
        if evt.GetString() == 'INET_E_RESOURCE_NOT_FOUND':
            return

        if self.first_error:
            self.web_view.LoadURL(self.URLs[1])
            self.first_error = False
        else:
            # there is some problem
            # it must be handled
            pass
#
#
# noinspection PyArgumentList,PyUnusedLocal
# noinspection PyPep8Naming
class BaseFrame(wx.Frame, Iconic):
    def __init__(self, title, pos=wx.DefaultPosition):

        wx.Frame.__init__(self, None, -1, title=title, pos=pos)
        Iconic.__init__(self, icon=ICON)
        #
        self.infile = kbc.config.get('directories', 'TEMPORAL')
        self.outfile = kbc.config.get('directories', 'TEMPOUT')
        #
        self.TEMPORAL = self.infile
        #
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.Bind(wx.EVT_CLOSE, self.OnCloseWindow)
        #
        self.pn_file = FilePanel(self, self.infile, self.outfile)
        self.Bind(wx.EVT_BUTTON, self.on_infile_selected, self.pn_file.bt_fin)
        self.Bind(wx.EVT_BUTTON, self.on_narrow_frame, self.pn_file.bt_dec)
        self.Bind(wx.EVT_BUTTON, self.on_wider_frame, self.pn_file.bt_inc)
        self.Bind(wx.EVT_BUTTON, self.on_outfile_selected,
                  self.pn_file.bt_fout)
        self.Bind(wx.EVT_BUTTON, self.on_search, self.pn_file.bt_search)
        self.Bind(wx.EVT_TEXT_ENTER, self.on_search, self.pn_file.tc_search)
        #
        self.pn_query = QueryPanel(self)
        self.Bind(wx.EVT_BUTTON, self.on_blast, self.pn_query.bt_blast)
        self.Bind(wx.EVT_TEXT, self.on_mod_entries, self.pn_query.tc_query)
        self.Bind(wx.EVT_BUTTON, self.on_save_infile, self.pn_query.bt_save)
        #
        self.pn_param = ParamPanel(self)
        props = self.pn_param.wright.properties
        self.Bind(wx.EVT_BUTTON, self.on_fasta_setup, props['Setup Fasta'])
        self.Bind(wx.EVT_BUTTON, self.on_color_keys, props['Color keys'])
        self.Bind(wx.EVT_BUTTON, self.on_about, props['About'])
        #
        self.Bind(wx.EVT_BUTTON, self.on_bt_calc,
                  self.pn_param.wcenter.calc.bt_calc)
        #
        self.pn_html = HtmlPanel(self)
        #
        self.Bind(EVT_WEBVIEW_NAVIGATING, self.before_navigate, self.pn_html.web_view)
        self.Bind(wx.EVT_BUTTON, self.on_save_xls, self.pn_html.bs_xcl)
        self.Bind(wx.EVT_BUTTON, self.on_search_google, self.pn_html.bt_google)
        self.Bind(wx.EVT_BUTTON, self.on_search_string, self.pn_html.bt_hvt)
        self.Bind(wx.EVT_BUTTON, self.on_increase, self.pn_html.bt_increase)
        self.Bind(wx.EVT_BUTTON, self.on_decrease, self.pn_html.bt_decrease)
        #
        sizer.Add(self.pn_file, flag=wx.EXPAND)
        sizer.Add(self.pn_query, flag=wx.EXPAND)
        sizer.Add(self.pn_param, flag=wx.EXPAND)
        sizer.Add(self.pn_html, 1, flag=wx.EXPAND)
        #
        self.SetSizer(sizer)
        self.Fit()
        #
        self.SetMinSize((480, 550))
        #
        if os.name == 'nt':
            self.SetSize((785, 550))
        else:
            self.SetSize((810, 550))
        #    
        self.log_text = self.pn_html.log_text

    def before_navigate(self, evt):
        """
        Actions before a page is actually loaded
        """
        URL = evt.GetURL()

        if '**' in URL:
            self.pn_html.web_view.Stop()
            self.on_prot_select(URL)

    def on_infile_selected(self, evt):
        """Reads a text archive and show contents in textCtrl"""
        #
        fpath, hay = open_file(self.pn_file.tc_fin)
        #
        if not fpath:
            return

        self.infile = fpath
        self.pn_query.tc_query.Clear()

        if hay:
            self.pn_query.tc_query.WriteText(readfile(fpath))
        else:
            self.pn_query.tc_query.WriteText('*Empty*')

        self.pn_query.bt_save.SetBackgroundColour('light grey')

    def on_outfile_selected(self, evt):
        """"""
        #
        fpath, hay = open_file(self.pn_file.tc_fout)
        if fpath:
            self.outfile = fpath

    def on_mod_entries(self, evt):
        self.pn_query.bt_save.SetBackgroundColour('red')

    def on_search(self, evt):
        pass

    def on_save_infile(self, evt):
        """
        Save the sequence(s) in textCtrl in a text file.
        If no file name is given, data is saved in C:/DIRQUIM/blast_temp
        """
        self.pn_query.bt_save.SetBackgroundColour('light grey')
        sequence = self.pn_query.tc_query.GetValue()
        infile = self.pn_file.tc_fin.GetValue()
        if infile and infile != self.TEMPORAL:
            fil = open(infile, 'w')
        else:
            fil = open(self.TEMPORAL, 'w')
        #
        fil.write(sequence)
        fil.close()

    def on_save_xls(self, evt):
        pass

    def on_search_google(self, evt):
        pass

    def on_search_string(self, evt):
        pass

    def on_narrow_frame(self, evt):
        pass

    def on_wider_frame(self, evt):
        pass

    def on_increase(self, evt):
        pass

    def on_decrease(self, evt):
        pass

    def on_bt_calc(self, evt):
        """Reads selected sequence and sends it to the calculator"""
        pass

    def on_fasta_setup(self, evt):
        pass

    # noinspection PyAttributeOutsideInit
    def on_color_keys(self, evt):
        if not Colors.exists:
            self.colors = Colors()
            self.colors.Show()
        else:
            self.colors.SetFocus()

    # noinspection PyAttributeOutsideInit
    def on_about(self, evt):
        if not About.exists:
            self.about = About()
            self.about.Show()
        else:
            self.about.SetFocus()

    def on_blast(self, evt):
        pass

    def blast_file_list(self, files):
        pass

    def search_results(self, infile, full_fpath):
        pass

    def on_prot_select(self, event):
        pass

    def OnCloseWindow(self, event):
        """delegated"""
        self.Destroy()
#
#
if __name__ == '__main__':

    MyParams.dbase_fnames.extend(["1", "1", "2"])
    class BlastFrame(BaseFrame):
        def __init__(self, title, pos=wx.DefaultPosition):
            """"""
            self.dictionary = {}
            self.tags = []
            self.enzyme = ''
            self.dbase = ''
            self.lang = ''
            self.has_changed = False
            # init_all()
            BaseFrame.__init__(self, title=title, pos=pos)

    app = wx.App()
    BlastFrame(title=kbc.TITLE).Show()
    app.MainLoop()
