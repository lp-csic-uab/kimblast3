KIMBLAST vs 2.09 (versió del 15 novembre de 2010)

Si llegeixes això és que SI EXISTEIX un arxiu anomenat,
'README.CAT.txt' amb instruccions generals d'aquest programa.

Aquest document podria no estar actualitzat. Cerca en README.txt

INSTRUCCIONS DETALLADES PER FORMATAR I INDEXAR NOVES BASES DE DADES:

1) Obtenir la nova base de dades en format FASTA. Existeixen 3 tipus de format fasta:
    a. - Antic Uniprot: "> name1 | name long ....."
    b. - Actual uniprot: "> sp | name1 | name long ....."
    c. - NCBI
2) Formata la base de dades (FormatDb):
    a. - Introduir el nom de la base de dades nova.
    b. - Seleccionar el fitxer fasta.
    c. - Especificar si la base és de proteïnes o de nucleòtids.
    d. - Especificar si el format és ASN1.
    e. - ACTUALITZAR: Actualitza la configuració del programa amb la nova base
         de dades.
    f. - FORMAT: Genera fitxer .nsq o .psq

    Esperar que es generi i completi el fitxer *.nsq/psq a C:\blast\DBs

3) Indexa la base de dades (IndexaDb):
    a. - Seleccionar el nom de la dades de base nova.
    b. - Seleccionar el tipus de fitxer fasta inclòs.
    c. - ACTUALITZAR: Actualitza la configuració del programa amb la nova base
         de dades.
    d. - INDEX: Genera fitxer .idx

    Esperar que es generi i completi el fitxer *.idx a C:\blast\DBs

4) Comprovar que KimBlast funciona amb la nova base de dades.

5) El programa guardarà la configuració amb la nova base de dades en tancar-se.



CARACTERÍSTIQUES I REVISIONS:

02/09 novembre 2010
- Corregit bug que feia perdre bons matches en el mode "mutar+unir".

08/02 novembre 2010
- Corregit bug introduït el 07/02/10, que no permetia formatar.
- Es passa a Mercurial (HG) com a sistema de control de canvis.

07/02 agost 2010
- Instal·lació del sistema SVN per a control del package.
- La lectura de text per a la calculadora ara neteja números i altres
  caràcters.

02/06 agost 2010
- Molts canvis, ull amb els bugs.
- S'ha canviat el nom de l'aplicació, que ara es diu KimBlast, nom més
  coherent amb el logo i amb que no es tracta de Blast.
- L'aplicació ara ve en un installer que proporciona les DLLs necessàries
- La calculadora s'ha millorat i s'han eliminat alguns bugs.
  Ara dóna directament el pes molecular. Si es vol calcular una
  modificació en els extrems primer es treu l'aigua i després s'hi afegeix
  la modificació.
  El pes molecular es dóna amb un decimal més en resposta a les noves
  prestacions ofertes per equips Orbitrap d'espectrometria de masses.
- S'ha actualitzat el codi per a les noves versions de wxPython i Python26.
  En Biopython 1.54 s'han de seguir corregint 'spark' i 'NCBIStandalone'
  per evitar la fallida de l'executable i el pantallazo de BLAST,respectivament.
- Queda sense resoldre la recerca amb el harvester. En llegir el resultat la
  pàgina web dóna errors. De vegades funciona i de vegades es penja i sempre és
  un conyàs. El problema és una de les webs on busca harvester, i on el
  programador ha fet servir un truc màgic per poder fer alguna cosa que en MS
  Internet Explorer no es pot fer. El tipus és molt llest però resulta que
  després el truc produeix errors de Javascript. A aquests llestos no els
  haurien de deixar programar coses d'ús públic.
  Fent servir l'Explorer directament sembla que l'error es filtra,
  però en wxPython no. Una solució seria obrir la pàgina en el navegador, però
  llavors perd la gràcia.

02/05 maig 2009
- Diversos problemes corregits:
	- Lectura d'arxius d'entrada i sortida.
	- Avís per salvar si es modifica la query.
	- Presentació d'URLs a la finestra de MS Internet Explorer.
	- Correcció de la presentació de la llista de digestió.
	- Ara té en compte el paràmetre gapped(T/F).
	- Error introduït en 04/02 que feia perdre una de les DBs del selector.
- Empaquetat amb Biopython 1.50 després de corregir codi de NCBIStandalone
  (Pantallazo cmd) i de parser (error d'execució).

2.04
- S'ha corregit un error en l'eina fix el qual anava eliminant la primera
  lletra de la seqüència en l'editor.
- Wordsize default és ara 2.
- S'han reorganitzat les classes de les bases de dades.
- Sembla que ara li costa compilar amb py2exe. Tarda un ou.
- Nota: ull, Biopyton vers 1.49 dóna problemes (pantallazo blast).
  Empaquetar o executar amb Biopython antic. L'actual paquet utilitza la v.1.45.

2.03
- Ara pot llegir tags de Lutefisk, del tipus [123]ABCD[444]SDF[111].
  En aquest cas el divideix en dos tags i emmagatzema la informació de masses
  a la capçalera de la seqüència.
- S'ha inclòs un mode "custom_bw" que només indica el match de BLAST, i sense
  colors.
- S'han corregit alguns bugs.
- S'ha inclòs iTRAQ a la calculadora.
- S'han inclòs rpsblast i blastpgp, encara que no sé molt bé com funcionen.

2.02
- Aquesta versió utilitza en nou control ActiveX per al plafó de presentació
  web.
- S'inclou una eina per a mutar les Leu (Peaks només genera Leu).

TODO vs 2.03

- Millorar el procés de formateig de databases:
  1) Crear una classe Formatador a part.
  2) Convertir els Dialogs en Frames perquè no es tanquin.


Aquests programes s'han fet perquè siguin útils. Si els utilitzes i no funcionen
completament al teu gust, fes-me saber que és el que creus que necessiten i
ho tindràs.

Joaquín Abián novembre 2010
