#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-
#
"""
kblast_cls_tag
"""
#
from kblast_funcs import find_extreme_aa, fill_match_gaps
from kblast_funcs import give_color, Enzyme
from commons.mass import get_mass
#
#
# noinspection PyUnresolvedReferences
class Tag:
    """"""

    attributes = ('prot_id1', 'prot_id2', 'names', 'prot_seq',
                  'diana', 'patron', 'subject', 'start', 'stop',
                  'offset', 'extra_coverage', 'score')

    def __init__(self, rec_id, enzyme=None):
        """Create a tag instance

        rec_id: tuple (sequence, file, tag_masses, truefalse)
        enzyme:

        """
        self.id = rec_id
        self.enzyme = enzyme
        self.side_length = 20
        for attribute in Tag.attributes:
            setattr(self, attribute, [])
        self.head_tail = {}
        self.head_tail_alpha = {}
    #
    def __str__(self):
        """"""
        txt1 = '<Tag for %s of size %i with member 1 :\n'
        txt2 = 'diana  \t%s\npatron\t%s\nsubject\t%s>'

        size = len(self.prot_id1)
        txt1 = txt1 % (self.id[0], size)

        if size:
            txt2 = txt2 % (self.diana[0], self.patron[0], self.subject[0])
        else:
            txt2 = 'Empty'

        return txt1 + txt2
    #
    def set_title_components(self, prot_id1, prot_id2, names, prot_seq):
        """"""
        self.prot_id1.append(prot_id1)
        self.prot_id2.append(prot_id2)
        self.names.append(names)
        self.prot_seq.append(prot_seq)
    #
    def set_tag(self, hsp, prot_seq):
        # noinspection SpellCheckingInspection
        """
        diana  -> the protein sequence
        patron -> hsp.match with gaps filled with '&' (gap in hsp.sbjct)
                                                  '*' (gap en hsp.query)
        subject  -> the matched protein sequence

                     AESPYIPSFDLGGDFG____GGGDFAD	the sequence

        Query: 3       SPYIPSFDLGGDFG____GGGDF 21	hsp.query
                       SPYIP   LGGDFG    GGGDF		hsp.match
        Sbjct: 18      SPYIP___LGGDFGGGDFGGGDF 37	hsp.sbjct/tag0.subject

        filled_match-> SPYIP&&&LGGDFG****GGGDF

        Diana        ACSPYIP---LGGDFGGGDFGGGDFGG 	tag0.diana
        Patron       $$SPYIP&&&LGGDFG****GGGDF$$	tag0.patron

        """
        sequence = self.id[0]
        filled_match = fill_match_gaps(hsp)
        # extra to color due to gaps in query
        extra_coverage = hsp.query.count('-')
        self.extra_coverage.append(extra_coverage)
        # to color at left of match
        extra_left = hsp.query_start - 1
        # to color at right of match
        extra_right = len(sequence) - hsp.query_end
        # to extend as far as the problem sequence
        patron = '$' * extra_left + filled_match + '$' * extra_right
        #
        # number of amino acid, no of the element in list.
        prot_start = hsp.sbjct_start - hsp.query_start + 1
        offset = 0
        if prot_start < 1:
            offset = 1 - prot_start
            prot_start = 1
        #
        prot_stop = hsp.sbjct_end + extra_right
        # takes into account the sequence can finish
        final = len(patron) - max(0, prot_stop - len(prot_seq))
        #
        diana = prot_seq[prot_start - 1:prot_stop]       # use -1 always in list
        diana_list = list(diana)

        patron = patron[offset:final]
        #
        for index, letter in enumerate(patron):
            if letter == '&':
                diana_list.insert(index, '-')
        #
        self.score.append(hsp.expect)
        self.diana.append(''.join(diana_list))
        self.patron.append(patron)
        self.subject.append(hsp.sbjct)
        self.start.append(prot_start)
        self.stop.append(prot_stop)
        self.offset.append(offset)
    #
    def get_link(self, item):
        """"""
        CODE = r'<b><a href=**%s*%s*%s >%s</a></b>'
        prot_id = self.prot_id1[item]
        start = self.start[item]
        stop = self.stop[item]

        prot_id_long = prot_id.center(14, '*')
        return CODE % (prot_id, start, stop, prot_id_long)
    #
    def get_head_and_tail(self, item):
        """
        <item> es el n�mero del match/protein (0, 1, 2,...)
        """
        side_length = self.side_length
        start = self.start[item]
        stop = self.stop[item]
        offset = self.offset[item]
        extra_coverage = self.extra_coverage[item]
        prot_seq = self.prot_seq[item]

        head = self.prot_seq[item][
                            stop:stop + side_length - extra_coverage].strip()
        tail_start = max(1, start - side_length)
        tail = prot_seq[tail_start - 1:start - 1]
        #
        head_index, tail_index = find_extreme_aa(head, tail, self.enzyme)

        # when query ends with the expected enzymatic C-term ->
        # no head should be added.
        # if head_index == 0 -> one aa is let at the head of the sequence
        # to remove all the aas in head -> head_index = -1
        # todo: except when forbidden
        if self.diana[item][-1] in Enzyme.cut[self.enzyme]:
            head_index = -1
        #
        dots_head = len(head) - head_index - 1
        dash_head = side_length - extra_coverage - head_index - dots_head - 1
        #
        # side_length - ((start-1) - offset))
        dash_tail = side_length - start + offset + 1
        dots_tail = tail_index
        #
        seq_tail = tail[tail_index:]
        seq_head = head[:head_index + 1]
        #
        head = seq_head + '.' * dots_head + '-' * dash_head
        tail = '-' * dash_tail + '.' * dots_tail + seq_tail
        #
        self.head_tail[item] = (head, tail)
        self.head_tail_alpha[item] = (seq_head, seq_tail)
    #
    def get_colored(self, item):
        """
        <item> es el n�mero del match/protein (0, 1, 2,...)
        """
        self.get_head_and_tail(item)
        patron = self.patron[item]
        diana = self.diana[item]
        #
        colored = give_color(patron, diana)
        #
        head, tail = self.head_tail[item]
        return tail + colored + head
    #
    def get_uncolored(self, item):
        """"""
        self.head_tail[item] = ("", "")
        self.head_tail_alpha[item] = ("", "")
        return self.subject[item]
    #
    def get_mass(self, item):
        """mass of the tryptic peptide defined by the query"""
        head, tail = self.head_tail_alpha[item]
        # noinspection PyArgumentEqualDefault
        return get_mass(tail + self.diana[item] + head, water=True)
    #
    def get_header(self):
        """"""
        return '<font color="green">%s %s %s %s</font>' % self.id
    #
    def get_hit_percent(self, item):
        """Return the percentage of identities on a blast alignment.

        Counts the number of letters in <patron>

        """
        len_seq = len(self.id[0])
        patron = self.patron[item]
        count = sum(item.isalpha() for item in patron)
        return int(count * 100 / len_seq)

    def tag_short_by(self, param):
        """Short tag matches by a given tag attribute <param>.

        TODO: Not finished. Currently shorting is by ... hit_percent/expectation?.

        """
        sort_keys = getattr(self, param)
        # noinspection PyTypeChecker
        to_sort = [(item, index) for index, item in enumerate(sort_keys)]
        to_sort.sort()
        indexes = [index for item, index in to_sort]
        # here sort all tag attributes using the index
        for attribute in Tag.attributes:   # ...etc
            alist = getattr(self, attribute)
            setattr(self, attribute, [alist[index] for index in indexes])


if __name__ == "__main__":

    id_ = ('P1234', 'file', '23-123', 'True')
    print('expected    -> <font color="green">P1234 file 23-123 True</font>')

    tag = Tag(id_)
    print('html header ->', tag.get_header())

    # TODO: A more complete test. These functions are important
