KIMBLAST vs 3.0 (versión 1 abril de 2011)

Si lees esto es que SI EXISTE un archivo llamado,
'README.ESP.txt' con instruciones generales de este programa.

Este documento podria no estar actualizado. Busca en README.txt 

INSTRUCCIONES DETALLADAS PARA FORMATEAR E INDEXAR NUEVAS DATABASES:

1) Obtener la nueva base de datos en formato FASTA. Existen 3 tipos de formato fasta:
    a.- Antiguo Uniprot: "> name1|name long....."
    b.- Actual uniprot: "> sp|name1|name long....."
    c.- ncbi

2) Formatea e indexa la base de datos:
    a.- Introducir el nombre de la base de datos nueva (Name).
    b.- Seleccionar el fichero fasta (Fasta File).
    c.- Especificar si la base es de proteínas o de nucleótidos (Format, type).
    d.- Cambiar el nombre del archivo .psq si se desea
    e.- Seleccionar el tipo de archivo fasta incluido (Index, type).
    f.- Cambiar el nombre del archivo .idx si se desea
    g.- Press Execute

    Esperar a que se genere y complete los ficheros
    *.nsq/psq  y *.idx en C:\blast\DBs en C:\blast\DBs

3) Comprobar que KimBLAST funciona con la nueva base de datos.

4) El programa guardará la configuración con la nueva base de datos al cerrarse.



CARACTERÍSTICAS Y REVISIONES:

3.00 abril 2011
- Nuevo dialogo para formateo e indexación de bases de datos que simplifica
 el procedimiento original.

2.09 noviembre 2010
- Corregido bug que hacia perder buenos matches en el modo “mutar+unir”.

2.08 noviembre 2010
- Corregido bug introducido en 2.07 que no permitía formatear.
- Se pasa a Mercurial (HG) como sistema de control de cambios.

2.07 agosto 2010
- Instalo sistema SVN para control del package.
- La lectura de texto para la calculadora ahora limpia números y otros
  caracteres.

2.06 agosto 2010
- Muchos cambios, ojo a bugs.
- Se ha cambiado el nombre de la aplicación que ahora se llama KimBlast que es
  más coherente con el logo y con que no es Blast.
- La aplicación ahora viene en un installer que proporciona las dlls necesarias
- La calculadora se ha mejorado y eliminado algunos bugs.
  Ahora da directamente el peso molecular. Si se quiere calcular una
  modificación en los extremos primero se quita el agua y luego se añade
  la modificación.
  El peso molecular se da con un decimal más en respuesta a las nuevas
  prestaciones ofrecidas por equipos Orbitrap de espectrometría de masas.
- Se ha actualizado el código para las nuevas versiones de wxPython y Python26.
  En Biopython 1.54 se tienen que seguir corrigiendo 'spark' y 'NCBIStandalone' 
  para evitar el fallo del ejecutable y el pantallazo de blast, respectivamente.
- Queda sin resolver la búsqueda con el Harvester. Al leer el resultado la
  página web da errores. A veces funciona y a veces se cuelga y siempre es un
  coñazo. El problema reside en una de las webs que busca Harvester donde el
  programador ha hecho un truco mágico para poder hacer algo que en MS Internet
  Explorer no se puede hacer. El tipo es muy listo pero resulta que luego el 
  truco produce errores de Javascript. A estos listos no les deberían
  dejar programar cosas de uso público.
  Con el Explorer directamente aparentemente el error se filtra,
  pero en wxPython no. Una solución sería mandar abrir la página en el
  navegador, pero entonces pierde la gracia.

2.05 mayo 2009
- Diversos problemas corregidos:
	- Lectura de archivos de entrada y salida.
	- Aviso de tener que salvar si se modifica la query.
	- Presentación de URLs en la ventana de MS Internet Explorer.
	- Corrección de la presentación de la lista de digestión.
	- Ahora tiene en cuenta el parámetro gapped(T/F).
	- Error introducido en 2.04 que perdía una de las DBs del selector.
- Compilado con Biopython 1.50 después de corregir código de NCBIStandalone
  (pantallazo cmd) y de parser (error de ejecución). 

2.04
- Se ha corregido un bug en la herramienta fix que iba eliminando la primera
  letra de la secuencia en el editor.
- Wordsize default es ahora 2.
- Se han reorganizado las clases de las bases de datos.
- Parece que ahora le cuesta compilar con py2exe. Tarda un huevo.
- Nota: ojo, Biopyton vers 1.49 da problemas (pantallazo blast).
  Empaquetar o ejecutar con Biopython viejo. El actual paquete usa la vs 1.45. 

2.03
- Ahora puede leer tags de Lutefisk tipo [123]ABCD[444]SDF[111].
  En este caso lo divide en dos tags y almacena la información de masas
  en la cabecera de la secuencia.
- Se ha incluido un modo "custom_bw" que únicamente da el match de blast
  y sin colores.
- Se han corregido algunos bugs.
- Se ha incluido iTRAQ en la calculadora.
- Se han incluido rpsblast y blastpgp aunque no se muy bien como funcionan.

2.02
- Esta versión utiliza en nuevo control ActiveX para la web.
- Se incluye una herramienta para mutar las Leu (PEAKS solo genera Leu).

TODO vs 2.03

- Mejorar el proceso de formateo de databases:
  1) Crear una clase Formateador a parte.
  2) Convertir los Dialogs en Frames para que no se cierren.


Estos programas se han hecho para que sean útiles. Si lo utilizas y no funciona
completamente a tu gusto, hazme saber que es lo que crees que necesitas y
lo tendrás.    

Joaquín Abián Noviembre 2010
