#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
kblast_cls_dbase
"""
#
import sqlite3
#
#
# noinspection PyMissingConstructor,SqlNoDataSourceInspection,SqlResolve
class BaseDataBase(dict):
    """A dictionary class with data from a sqlite3 database."""
    def __init__(self, sql_dbase):
        """Open BaseDataBase connexion and set attributes.

        Attributes:
        sql_dbase : the sqlite file
        fasta_file : fasta original to fill de dbase
        dbase_type :  dbase type

        """
        self.sql_dbase = sql_dbase
        self.fasta_file = ""
        self.dbase_type = ""
        self.open()

    def __len__(self):
        self.cur.execute("""select count(*) from sqltable""")
        nrows = self.cur.fetchone()[0]
        return nrows

    def __getitem__(self, ref):
        item = (ref,)
        self.cur.execute("""select * from sqltable where id = ?""", item)
        row = self.cur.fetchone()
        if row is None:
            raise KeyError()
        else:
            return row

    def __iter__(self):
        """"""
        self.cur.execute("""select * from sqltable""")
        for item in self.cur:
            yield item

    def __setitem__(self, item, value):
        print("Setting a value manually is not possible")

    # noinspection PyAttributeOutsideInit
    def open(self):
        """Open connection with sqlite dbase"""
        self.conn = sqlite3.connect(self.sql_dbase)
        self.cur = self.conn.cursor()

    def fill(self, fasta_file, dbase_type='uniprot'):
        """Fill and index database.

        fasta_file is the input fasta file

        """
        pass

    def close(self):
        self.conn.commit()
        self.cur.close()
#
#
if __name__ == "__main__":

    pass
