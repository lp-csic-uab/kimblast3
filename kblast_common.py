#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
kblast_common
"""
import os
import sys
import configparser
#
TITLE = ' KimBlast vs.5.1.1'
#
WILDCARD = "HTML (*.html)|*.html|"  \
           "Text (*.txt)|*.txt|"   \
           "XML (*.xml)|*.xml"
#
if getattr(sys, 'frozen', False):
    # If the application is run as an executable, pyInstaller bootloader
    # extends the sys module by a flag frozen=True.
    # The app path can be read from sys.executable'.
    APP_DIR = sys.executable
else:
    APP_DIR = os.path.dirname(__file__)
#
# The folder from where kimblast.py
# is running is kimblast.exe
# this should be corrected to the container folder
# in order to read configuration files
if APP_DIR.endswith('.exe'):
    APP_DIR = os.path.split(APP_DIR)[0]
#
_COMMONS_FILE = {'nt': 'config_common_win.cfg',
                 'posix': 'config_common_posix.cfg'}
#
#
_DIRECTORIES = {'DIRQUIM': os.path.join(APP_DIR, "data"),
                'TEMPORAL': os.path.join(APP_DIR, "data", "blast_default"),
                'TEMPOUT':  os.path.join(APP_DIR, "data", "blast_out"),
                'DB_CONF_FILE': os.path.join(APP_DIR, "data", "config.txt"),
                'SAVEXCEL': os.path.join(APP_DIR, "data", "out.htm"),
                'BATCHFILE': os.path.join(APP_DIR, "data", "batchfile"),
                'BLASTPATH': "",
                'DBPATH': os.path.join(APP_DIR, "data", "demo_databases")
                }
#
GOOGLE = {'REFERER': 'LP CSIC/UAB search by Joaquin Abian',
          'SITES': 'uniprot.org|wikipedia.org',
          'USER_IP': '',
          'API_KEY': ''}
#
#
config = configparser.ConfigParser()
#
config.add_section('directories')
for option, value in _DIRECTORIES.items():
    config.set('directories', option, value)
#
config.add_section('google')
for option, value in GOOGLE.items():
    config.set('google', option, value)
#
config.read(os.path.join(APP_DIR, _COMMONS_FILE[os.name]))
config.read(os.path.join(APP_DIR, 'params', 'blastp-s_params.cfg'))
#
# test
# config.write(open('full_configuration_file', 'w'))
